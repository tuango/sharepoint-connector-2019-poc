package com.capgemini.connector.sharepoint.constants;

public interface SharePointConstants {
	
	interface Attachment {
		static final String ID = "odata.id";
		static final String FILE_NAME = "FileName";
		static final String SERVER_RELATIVE_URL = "ServerRelativeUrl";
		static final String FILE_TYPE = "sharepoint";
		static final String IS_BINARY_FILE = "isBinaryFile";
		static final String FILE_CONTENT = "content";
	}
	
}
