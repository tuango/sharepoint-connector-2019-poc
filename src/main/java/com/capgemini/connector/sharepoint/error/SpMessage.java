package com.capgemini.connector.sharepoint.error;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.ToString;

/**
 * {
 *    "lang": "en-US",
 *    "value": "List 'tracker10238123' does not exist at site with URL 'http://ecollaborative-int.capgemini.com/sites/latam/proj39826'."
 * }
 */
@Data
@ToString(callSuper=true)
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SpMessage {
	private String lang;
	private String value;
}
