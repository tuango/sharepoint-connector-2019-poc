package com.capgemini.connector.sharepoint.error;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;

/**
 * 	{
 * 	    "odata.error": {
 * 	        "code": "-1, System.ArgumentException",
 * 	        "message": {
 * 	            "lang": "en-US",
 * 	            "value": "List 'tracker10238123' does not exist at site with URL 'http://ecollaborative-int.capgemini.com/sites/latam/proj39826'."
 * 	        }
 * 	    }
 * 	}
 */
@Data
@ToString(callSuper=true)
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SpError {
	
	// SpError structure when we receive a functionnal error (like 'tracker10238123' does not exist)
	@JsonProperty("odata.error")
	private SpODataError oDataError;
	
	// SpError structure when we receive a technical error (like The server was unable to process the request due to an internal error...)
	@JsonProperty("error_description")
	private String errorDescription;
}
