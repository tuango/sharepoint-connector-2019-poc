package com.capgemini.connector.sharepoint.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public class SharePointClientException extends Exception {
	private static final long serialVersionUID = -3988410142661985124L;

	private HttpStatus status;
	private String errorCode;
	private Long stickyNoteId;//sticky note id is used to rollback to previous position in front-office.
	private String eventType;
	private String action;
	
	public SharePointClientException(HttpStatus status, String errorCode, String message, String eventType, Throwable cause) {
		super(message, cause);
		this.status = status;
		this.errorCode = errorCode;
		this.eventType = eventType;
	}
}
