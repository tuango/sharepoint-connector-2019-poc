package com.capgemini.connector.sharepoint.exception;

/**
 * Raised by SharePointUrlInformationExtractor when it can't find the
 * url-extractor-regex associated with the hostname of the SharePoint url.
 * 
 * @author lhauspie
 */
public class SharePointUrlInformationExtractionException extends Exception {
	private static final long serialVersionUID = 843652088851159328L;

	public SharePointUrlInformationExtractionException() {
		super();
	}

	public SharePointUrlInformationExtractionException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public SharePointUrlInformationExtractionException(String message, Throwable cause) {
		super(message, cause);
	}

	public SharePointUrlInformationExtractionException(String message) {
		super(message);
	}

	public SharePointUrlInformationExtractionException(Throwable cause) {
		super(cause);
	}
}
