package com.capgemini.connector.sharepoint.exception;

/**
 * Raised by JsonWebTokenGeneratorFactory when it can't find the
 * JsonWebTokenGenerator associated with the hostname of the project url.
 * 
 * @author lhauspie
 *
 */
public class SharePointFarmNotFoundException extends Exception {
	private static final long serialVersionUID = 3830245795944822118L;

	public SharePointFarmNotFoundException() {
		super();
	}

	public SharePointFarmNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public SharePointFarmNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public SharePointFarmNotFoundException(String message) {
		super(message);
	}

	public SharePointFarmNotFoundException(Throwable cause) {
		super(cause);
	}
}
