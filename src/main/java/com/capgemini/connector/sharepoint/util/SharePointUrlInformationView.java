package com.capgemini.connector.sharepoint.util;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SharePointUrlInformationView {
	private String engagementUrl;
	private String trackerId;
}
