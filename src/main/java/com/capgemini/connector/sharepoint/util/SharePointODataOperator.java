package com.capgemini.connector.sharepoint.util;

/**
 * Constants to build REST API urls to get data from SharePoint
 * 
 * @author lhauspie
 */
// FIXME : see if there's existed OData 3.0 framework ?
public class SharePointODataOperator {
	
	private static final String ODATA_SELECT = "$select";
	private static final String ODATA_FILTER = "$filter";
	private static final String ODATA_EXPAND = "$expand";
	private static final String ODATA_ORDERBY = "$sorderby";
	
	private String select;
	private String filter;
	private String expand;
	private String orderby;
	
	public SharePointODataOperator(String select, String filter, String expand, String orderby) {
		super();
		this.select = select;
		this.filter = filter;
		this.expand = expand;
		this.orderby = orderby;
	}
	
	public String getSelect() {
		return select;
	}
	public String getFilter() {
		return filter;
	}
	public String getExpand() {
		return expand;
	}
	public String getOrderby() {
		return orderby;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		boolean firstODataOperator = Boolean.TRUE;
		if (select != null) {
			firstODataOperator = append(builder, firstODataOperator, ODATA_SELECT, select);
		}
		if (filter != null) {
			firstODataOperator = append(builder, firstODataOperator, ODATA_FILTER, filter);
		}
		if (expand != null) {
			firstODataOperator = append(builder, firstODataOperator, ODATA_EXPAND, expand);
		}
		if (orderby != null) {
			firstODataOperator = append(builder, firstODataOperator, ODATA_ORDERBY, orderby);
		}
		
		return builder.toString();
	}
	
	private boolean append(StringBuilder builder, boolean firstODataOperator, String oDataOperator, String oDataOperatorContent) {
		if (firstODataOperator) {
			builder.append("?");
		} else {
			builder.append("&");
		}
		builder.append(oDataOperator).append("=").append(oDataOperatorContent);
		return false;
	}
}
