package com.capgemini.connector.sharepoint.util;

import java.net.URI;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.capgemini.connector.sharepoint.exception.SharePointUrlInformationExtractionException;

/**
 * Utility class allowing to extract engagementUrl and trackerId from a SharePoint url
 * This utility class allows you to extract the engagementUrl and the trackerId from any SharePoint url.
 * This class is instantiated with all the regular expressions of the SharePoint url related to each farm to know the good regex to use to extract data.
 * Doing by this way, we can specify for each farm what is the format of the url and where we can find projectUrl and trackerId.
 * I used the concept of capturing groups in regex.<br/>
 * Example : <br/>
 * url : https://ecollaborative-int.capgemini.com/sites/latam/proj30989/Lists/tracker95196/TrackerHomeForm.aspx<br/>
 * regex : (https?://ecollaborative-int.capgemini.com/sites/[^/]+/[^/]+)/Lists/([^/]+)(/.*)?<br/>
 * projectUrl (matcher.group(1)) = https://ecollaborative-int.capgemini.com/sites/latam/proj30989<br/>
 * trackerId (matcher.group(2)) = tracker95196
 * @author lhauspie
 *
 */
public class SharePointUrlInformationExtractor {
	
	/**
	 * Key : SharePoint farm Hostname
	 * Value : Regular Expression to extract engagementUrl and trackerId
	 */
	private Map<String, String> regularExpressions;

	public SharePointUrlInformationExtractor(Map<String, String> regularExpressions) {
		this.regularExpressions = regularExpressions;
	}
	
	public SharePointUrlInformationView extract(URI sharePointUrl) throws SharePointUrlInformationExtractionException {
		String regex = getRegularExpressionByHostname(sharePointUrl.getHost());
		
		final Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(sharePointUrl.toString());
		if (!matcher.matches()) {
			throw new SharePointUrlInformationExtractionException("The provided url " + sharePointUrl + " doesn't matches the specified url regex " + regex);
		}
		return new SharePointUrlInformationView(matcher.group(1), matcher.group(2));
	}
	
	private String getRegularExpressionByHostname(String hostname) throws SharePointUrlInformationExtractionException {
		String regularExpressionByHostname = regularExpressions.get(hostname);
		if (null == regularExpressionByHostname) {
			throw new SharePointUrlInformationExtractionException(
					"Can't find the url regular expression of the farm corresponding to the hostname \"" + hostname + "\" to extract engagementUrl and trackerId");
		}
		return regularExpressionByHostname;
	}
}
