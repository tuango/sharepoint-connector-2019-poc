package com.capgemini.connector.sharepoint.util;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.http.entity.ContentType;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpMessageConverterExtractor;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import com.capgemini.connector.sharepoint.constants.SharePointConstants;
import com.capgemini.connector.sharepoint.error.SpError;
import com.capgemini.connector.sharepoint.exception.SharePointClientException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SharePointClientUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(SharePointClientUtil.class);

	private static final int REAML_SIZE = 36;

	private static ObjectMapper objectMapper = new ObjectMapper();


	public static JSONObject getRequest(final String url, Map<String, Object> urlParams, RestTemplate restTemplate, final String token)
			throws JSONException, SharePointClientException {
		LOGGER.info("getRequest(url:'{}', urlParams:'{}', restTemplate:'{}', token:'{}')", url, urlParams, restTemplate, token);
		return sendRequest(HttpMethod.GET, url, urlParams, restTemplate, token, null, null, null);
	}

	public static JSONObject getRequest(final String url, RestTemplate restTemplate, final String token)
			throws JSONException, SharePointClientException {
		LOGGER.info("getRequest(url:'{}', restTemplate:'{}', token:'{}')", url, restTemplate, token);
		return getRequest(url, MapUtils.EMPTY_MAP, restTemplate, token);
	}

	public static JSONObject postRequest(final String url, RestTemplate restTemplate, final String token)
			throws JSONException, SharePointClientException {
		LOGGER.info("postRequest(url:'{}', restTemplate:'{}', token:'{}')", url, restTemplate, token);
		return sendRequest(HttpMethod.POST, url, MapUtils.EMPTY_MAP, restTemplate, token, null, null, null);
	}

	public static JSONObject postRequest(final String url, RestTemplate restTemplate, final String token, final JSONObject jsonObject) throws JSONException, SharePointClientException {
		LOGGER.info("postRequest(url:'{}', restTemplate:'{}', token:'{}', jsonObject:'{}')", url, restTemplate, token);
		return sendRequest(HttpMethod.POST, url, MapUtils.EMPTY_MAP, restTemplate, token, null, jsonObject, null);
	}

	public static JSONObject postRequest(final String url, RestTemplate restTemplate, final String token,
										 final String formDigest, final JSONObject jsonObject, final String etag) throws JSONException, SharePointClientException {
		
		if(!isBinaryFile(jsonObject)) {
			LOGGER.info("postRequest(url:'{}', restTemplate:'{}', token:'{}', formDigest:'{}', jsonObject:'{}', etag:'{}')", url, restTemplate, token, formDigest, jsonObject, etag);	
		} else {
			// don't need to write content of file into log.
			LOGGER.info("postRequest(url:'{}', restTemplate:'{}', token:'{}', formDigest:'{}', etag:'{}')", url, restTemplate, token, formDigest, etag);
		}
		
		return sendRequest(HttpMethod.POST, url, MapUtils.EMPTY_MAP, restTemplate, token, formDigest, jsonObject, etag);
	}

	public static JSONObject patchRequest(final String url, RestTemplate restTemplate, final String token,
										  final String formDigest, final JSONObject jsonObject, final String etag) throws JSONException, SharePointClientException {
		LOGGER.info("patchRequest(url:'{}', restTemplate:'{}', token:'{}', formDigest:'{}', jsonObject:'{}', etag:'{}')", url, restTemplate, token, formDigest, jsonObject, etag);
		return sendRequest(HttpMethod.PATCH, url, MapUtils.EMPTY_MAP, restTemplate, token, formDigest, jsonObject, etag);
	}

	public static JSONObject deleteRequest(final String url, RestTemplate restTemplate, final String token,
										   final String formDigest, final String etag) throws JSONException, SharePointClientException {
		LOGGER.info("deleteRequest(url:'{}', restTemplate:'{}', token:'{}', formDigest:'{}', etag:'{}')", url, restTemplate, token, formDigest, etag);
		return sendRequest(HttpMethod.DELETE, url, MapUtils.EMPTY_MAP, restTemplate, token, formDigest, null, etag);
	}

	/**
	 * Send the request to the <code>url</code> with the <code>httpMethod</code>
	 * by using <code>restTemplate</code> with the <code>jsonObject</code> in
	 * body.<br/>
	 * The HTTP Headers are : <br/>
	 * <ul>
	 * <li><b>Authorization</b> : containing "Bearer <code>token</code>"</li>
	 * <li><b>Accept</b> : containing "application/json; odata=verbose"</li>
	 * <li><b>Content-Type</b> : containing "application/json; odata=verbose"
	 * </li>
	 * <li><b>X-HTTP-Method</b> : containing <code>httpMethod.toString()</code>
	 * </li>
	 * <li><b>X-RequestDigest</b> : containing <code>formDigest</code></li>
	 * <li><b>IF-MATCH</b> : containing <code>etag</code></li>
	 * </ul>
	 *
	 * @param httpMethod
	 *            the method of the query to send (GET, POST, PATCH, DELETE)
	 * @param url
	 *            the url to send the query
	 * @param restTemplate
	 *            the restTemplate to use to send the query
	 * @param token
	 *            the Json Web Token you generate to call SharePoint REST API
	 * @param formDigest
	 *            the formDigest you retrieved from SharePoint be calling
	 *            /_api/contextinfo in by POST method
	 * @param jsonObject
	 *            the JSONObject you want to send to SharePoint.
	 * @param etag
	 *            the version of the jsonObject to post/patch. If the ETag is
	 *            not the last version of the SharePoint item, SharePoint will
	 *            deny the merge.
	 * @return the JSONObject returned by SharePoint
	 * @throws JSONException
	 *             if this method is not able to transform httpResponse to
	 *             JSONObject
	 */
	private static JSONObject sendRequest(HttpMethod httpMethod, final String url, Map<String, Object> urlParams, RestTemplate restTemplate,
										  final String token, final String formDigest, final JSONObject jsonObject, final String etag)
			throws JSONException, SharePointClientException {
		
		if(isBinaryFile(jsonObject)) {
			// don't need to write content of file into log.
			LOGGER.info("sendRequest(httpMethod:'{}', url:'{}', urlParams:'{}', restTemplate:'{}', token:'{}', formDigest:'{}', etag:'{}')", httpMethod, url, urlParams, restTemplate, token, formDigest, etag);
		} else {
			LOGGER.info("sendRequest(httpMethod:'{}', url:'{}', urlParams:'{}', restTemplate:'{}', token:'{}', formDigest:'{}', jsonObject:'{}', etag:'{}')", httpMethod, url, urlParams, restTemplate, token, formDigest, jsonObject, etag);		
		}
	
		final HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Bearer " + token);
		headers.add("Accept", ContentType.APPLICATION_JSON.getMimeType());
		headers.add("Content-Type", ContentType.APPLICATION_JSON.getMimeType());
		headers.add("X-HTTP-Method", httpMethod.toString());
		headers.add("X-RequestDigest", formDigest);
		if (etag != null) {
			headers.add("IF-MATCH", etag);
		}

		SharePointRequestCallback sharePointRequestCallback = null;
		
		if (isBinaryFile(jsonObject)) {
			sharePointRequestCallback = new SharePointRequestCallback(headers, null, jsonObject.get(SharePointConstants.Attachment.FILE_CONTENT));
		} else {
			
			sharePointRequestCallback = new SharePointRequestCallback(headers, jsonObject, null);
		}

		ResponseExtractor<String> responseExtractor = new HttpMessageConverterExtractor<String>(String.class, restTemplate.getMessageConverters());
		String response = null;
		try {
			response = restTemplate.execute(getUri(url, urlParams), httpMethod, sharePointRequestCallback, responseExtractor);
		} catch (HttpStatusCodeException e) {
			throwTranslatedException(e);
		}
		return response == null ? new JSONObject() : new JSONObject(response);
	}

	/**
	 * Transform the url to an expanded URI if needed.
	 * It's needed if the url contains a '$' character.
	 *
	 * @param url the url to transform to an expanded URI.
	 * @return the expanded URI corresponding to the url
	 */
	private static URI getUri(String url, Map<String, Object> urlParams) {
		URI result;
		// we override the default behavior only if the URI contains params '?'
		if (url.contains("?") && MapUtils.isEmpty(urlParams)) {
			try {
				result = new URI(url);
			} catch (URISyntaxException ex) {
				throw new IllegalArgumentException("Invalid URL after inserting base URL: " + url, ex);
			}
		} else {
			Map<String, Object> uriVariables = urlParams == null ? MapUtils.EMPTY_MAP : urlParams;
			String uriTemplate = buildUriTemplate(url, uriVariables);
			result = new UriTemplate(uriTemplate).expand(uriVariables);
		}

		LOGGER.info("getUri(url:'{}', urlParams:'{}') returns '{}'", url, urlParams, result);
		return result;
	}

	/**
	 * if urlVariables contains {"$filter"="catgory ed 'A3 & 1'", "$select"="Id, priority, category"} the returned uriTemplate will be "&lt;url&gt;?$filter={$filter}&$select={$select}"
	 * @param url the original url without any parameters
	 * @param urlVariables the map of parameters with paramName as Key and paramValue as Value
	 * @return retourne un template uri avec les parametres spécifier dans urlVariables. si urlVariable
	 */
	private static String buildUriTemplate(String url, Map<String, Object> urlVariables) {
		StringBuilder stringBuilder = new StringBuilder(url);
		boolean firstParam = true;
		if (MapUtils.isNotEmpty(urlVariables)) {
			for (String paramKey : urlVariables.keySet()) {
				firstParam = append(stringBuilder, firstParam, paramKey, "{" + paramKey + "}");
			}
		}
		return stringBuilder.toString();
	}

	private static boolean append(StringBuilder builder, boolean firstParam, String paramKey, String paramValue) {
		if (firstParam) {
			builder.append("?");
		} else {
			builder.append("&");
		}
		builder.append(paramKey).append("=").append(paramValue);
		return false;
	}

	//
	/**
	 * Retrieves the realm by calling SharePoint farm with the specified url.
	 *
	 * @param siteUri
	 *            the url of the site you want to retrieve the realm
	 * @return The realm of the SharePoint farm
	 */
	public static String getRealmFromTargetSite(URI siteUri) {
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", "Bearer");
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.execute(siteUri + "/_vti_bin/client.svc", HttpMethod.POST, new SharePointRequestCallback(headers, null, null), null);
		} catch (HttpStatusCodeException e) {
			
			LOGGER.info("Execute http request failed", e);
			
			if (e.getResponseBodyAsString() == null) {
				return null;
			}

			String bearerResponseHeader = e.getResponseHeaders().get("WWW-Authenticate").toString();
			if (StringUtils.isEmpty(bearerResponseHeader)) {
				return null;
			}

			String bearer = "Bearer realm=\"";
			int bearerIndex = bearerResponseHeader.indexOf(bearer);
			if (bearerIndex < 0) {
				return null;
			}

			int realmIndex = bearerIndex + bearer.length();
			if (bearerResponseHeader.length() >= realmIndex + REAML_SIZE) {
				return bearerResponseHeader.substring(realmIndex, realmIndex + REAML_SIZE);
			}
		}
		return null;
	}

	private static void throwTranslatedException(HttpStatusCodeException exception) throws SharePointClientException {
		String responseBody = exception.getResponseBodyAsString();

		LOGGER.info("throwTranslatedException(HttpStatusCodeException : '{}')", exception.toString());
		LOGGER.info("exception.statusCode : '{}'", exception.getStatusCode());
		LOGGER.info("exception.responseBody : '{}'", responseBody);

		try {
			// try to translate the HttpStatusCodeException to SharePointClient
			SpError spError = objectMapper.reader(SpError.class).readValue(responseBody);
			LOGGER.info("spError : '{}'", spError);

			HttpStatus status = exception.getStatusCode();
			String errorCode = null;
			String message = null;
			if (spError.getODataError() != null) {
				errorCode = spError.getODataError().getCode();
				message = spError.getODataError().getMessage().getValue();
			} else {
				errorCode = String.valueOf(exception.getStatusCode().value());
				message = spError.getErrorDescription();
			}
			throw new SharePointClientException(status, errorCode, message, null, null);
		} catch (IOException e) {
			// throw again the input exception as is if we don't manage to translate it
			LOGGER.error("Error during parsing error with response body : {}", responseBody, e);
			throw exception;
		}
	}
	
	private static boolean isBinaryFile(JSONObject jsonObject) {
		return (jsonObject != null && jsonObject.has(SharePointConstants.Attachment.IS_BINARY_FILE));
	}
}
