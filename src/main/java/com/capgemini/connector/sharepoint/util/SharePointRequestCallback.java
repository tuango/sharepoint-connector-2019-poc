package com.capgemini.connector.sharepoint.util;

import java.io.IOException;

import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.web.client.RequestCallback;

public class SharePointRequestCallback implements RequestCallback {
	private HttpHeaders headers;
	private JSONObject jsonObject;
	private Object fileObject;

	public SharePointRequestCallback(HttpHeaders headers, JSONObject jsonObject, Object fileObject) {
		super();
		this.headers = headers;
		this.jsonObject = jsonObject;
		this.fileObject = fileObject;
	}

	@Override
	public void doWithRequest(ClientHttpRequest request) throws IOException {
		if (jsonObject != null) {
			request.getBody().write(jsonObject.toString().getBytes());
		} else if(fileObject != null) {
			request.getBody().write((byte[]) fileObject);
		}
		
		request.getHeaders().putAll(headers);
	}
}

