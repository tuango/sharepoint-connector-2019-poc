package com.capgemini.connector.sharepoint.util;

/**
 * Tool class to build REST API urls to get data from SharePoint
 * 
 * @author lhauspie
 */
public class SharePointUrlMaker {

	public static String getListByTitle(String listTitle) {
		return "/_api/lists/getByTitle('" + listTitle + "')";
	}
	public static String getListByTitleRoleAssignments(String listTitle) {
		return getListByTitle(listTitle) + "/roleAssignments";
	}
	public static String getListByTitleItems(String listTitle) {
		return getListByTitle(listTitle) + "/items";
	}
	public static String getListByTitleFields(String listTitle) {
		return getListByTitle(listTitle) + "/fields";
	}
	public static String getListByTitleItemById(String listTitle, int itemId) {
		return getListByTitleItems(listTitle) + "(" + itemId + ")";
	}
	public static String getListByGuid(String listGuid) {
		return "/_api/lists(guid'" + listGuid + "')";
	}
	public static String getListByGuidRoleAssignments(String listGuid) {
		return getListByGuid(listGuid) + "/roleAssignments";
	}
	public static String getListByGuidItems(String listGuid) {
		return getListByGuid(listGuid) + "/items";
	}
	public static String getListByGuidFields(String listGuid) {
		return getListByGuid(listGuid) + "/fields";
	}
	public static String getListByGuidItemById(String listGuid, int itemId) {
		return getListByGuidItems(listGuid) + "(" + itemId + ")";
	}
	public static String getCurrentUser() {
		return "/_api/web/currentUser";
	}
	public static String getSiteUsers() {
		return "/_api/web/siteUsers";
	}
	public static String getContextInfo() {
		return "/_api/contextinfo";
	}
	
	// With OData Operators
	public static String getListByTitle(String listTitle, SharePointODataOperator oDataOperator) {
		return append(getListByTitle(listTitle), oDataOperator);
	}
	public static String getListByTitleRoleAssignments(String listTitle, SharePointODataOperator oDataOperator) {
		return append(getListByTitleRoleAssignments(listTitle), oDataOperator);
	}
	public static String getListByTitleItems(String listTitle, SharePointODataOperator oDataOperator) {
		return append(getListByTitleItems(listTitle), oDataOperator);
	}
	public static String getListByTitleFields(String listTitle, SharePointODataOperator oDataOperator) {
		return append(getListByTitleFields(listTitle), oDataOperator);
	}
	public static String getListByTitleItemById(String listTitle, int itemId, SharePointODataOperator oDataOperator) {
		return append(getListByTitleItemById(listTitle, itemId), oDataOperator);
	}
	public static String getListByGuid(String listGuid, SharePointODataOperator oDataOperator) {
		return append(getListByGuid(listGuid), oDataOperator);
	}
	public static String getListByGuidItems(String listGuid, SharePointODataOperator oDataOperator) {
		return append(getListByGuidItems(listGuid), oDataOperator);
	}
	public static String getListByGuidFields(String listTitle, SharePointODataOperator oDataOperator) {
		return append(getListByGuidFields(listTitle), oDataOperator);
	}
	public static String getListByGuidItemById(String listGuid, int itemId, SharePointODataOperator oDataOperator) {
		return append(getListByGuidItemById(listGuid, itemId), oDataOperator);
	}
	public static String getCurrentUser(SharePointODataOperator oDataOperator) {
		return append(getCurrentUser(), oDataOperator);
	}
	public static String getSiteUsers(SharePointODataOperator oDataOperator) {
		return append(getSiteUsers(), oDataOperator);
	}
	


	private static String append(String str, SharePointODataOperator oDataOperator) {
		return str + (oDataOperator != null ? oDataOperator.toString() : "");
	}
}
