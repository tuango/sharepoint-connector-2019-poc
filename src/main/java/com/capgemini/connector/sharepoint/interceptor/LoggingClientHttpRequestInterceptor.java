package com.capgemini.connector.sharepoint.interceptor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class LoggingClientHttpRequestInterceptor implements ClientHttpRequestInterceptor {
	
	private static final Logger log = LoggerFactory.getLogger(LoggingClientHttpRequestInterceptor.class);
	
	@Override
	public ClientHttpResponse intercept(final HttpRequest request, final byte[] body, final ClientHttpRequestExecution execution) throws IOException {
		if (!log.isInfoEnabled(LoggingUtils.REST_MARKER)) {
			return execution.execute(request, body);
		}
		
		traceRequest(request, body);
		final ClientHttpResponse response = execution.execute(request, body);
		traceResponse(response);
		
		return response;
	}
	
	private void traceRequest(final HttpRequest request, final byte[] body) throws IOException {
		log.info(LoggingUtils.REST_MARKER, "==========================request begin=============================================");
		log.info(LoggingUtils.REST_MARKER, "Method      : {}", request.getMethod());
		log.info(LoggingUtils.REST_MARKER, "URI         : {}", request.getURI());
		log.info(LoggingUtils.REST_MARKER, "Headers     : {}", request.getHeaders());
		log.info(LoggingUtils.REST_MARKER, "Request body: {}", new String(body, "UTF-8"));
		log.info(LoggingUtils.REST_MARKER, "==========================request end===============================================");
	}
	
	private void traceResponse(final ClientHttpResponse response) throws IOException {
		// log only org.springframework.http.client.BufferingClientHttpResponseWrapper
		// because it's the only one we can read twice
		// if we read the response of an other ClientHttpResponse type, we got an empty response
		if (!"org.springframework.http.client.BufferingClientHttpResponseWrapper".equals(response.getClass().getName())) {
			return;
		}
		final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getBody(), "UTF-8"));
		List<String> lines = new ArrayList<String>();
		for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {
			lines.add(line);
		}
		log.info(LoggingUtils.REST_MARKER, "==========================response begin============================================");
		log.info(LoggingUtils.REST_MARKER, "Status code  : {}", response.getStatusCode());
		log.info(LoggingUtils.REST_MARKER, "Status text  : {}", response.getStatusText());
		log.info(LoggingUtils.REST_MARKER, "Headers      : {}", response.getHeaders());
		log.info(LoggingUtils.REST_MARKER, "Response body: {}", String.join("/n", lines));
		log.info(LoggingUtils.REST_MARKER, "==========================response end==============================================");
	}
}
