package com.capgemini.connector.sharepoint.interceptor;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StopWatch;

public class PerformanceClientHttpRequestInterceptor implements ClientHttpRequestInterceptor {
	
	private static final Logger log = LoggerFactory.getLogger(PerformanceClientHttpRequestInterceptor.class);
	
	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
		if (!log.isInfoEnabled(LoggingUtils.PERF_MARKER)) {
			return execution.execute(request, body);
		}
		
		final StopWatch stopWatch = new StopWatch(generateTaskName(request));
		stopWatch.start("ClientHttpRequestExecution.execute");
		final ClientHttpResponse response = execution.execute(request, body);
		stopWatch.stop();
		
		log.info(LoggingUtils.PERF_MARKER, stopWatch.shortSummary());
		
		return response;
	}
	
	private String generateTaskName(HttpRequest request) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(request.getMethod());
		stringBuilder.append(' ');
		stringBuilder.append(request.getURI());
		return stringBuilder.toString();
	}
}
