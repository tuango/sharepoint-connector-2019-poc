package com.capgemini.connector.sharepoint.interceptor;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

public final class LoggingUtils {
	
	/**
	 * Marker for all performance logs.
	 */
	public static final Marker PERF_MARKER = MarkerFactory.getMarker("[PERF]");
	
	/**
	 * Marker for all REST logs.
	 */
	public static final Marker REST_MARKER = MarkerFactory.getMarker("[REST]");
}
