package com.capgemini.connector.sharepoint.token;

import java.util.Date;

import org.springframework.util.Assert;

public class JsonWebToken {
	private Date expirationDate;
	private String token;
	
	// allow to anticipate the token expiration
	// if we check the token 1 millis before the due time, it will be expired when we use it
	private long expiredBefore = 10000;
	
	public JsonWebToken(Date expirationDate, String token) {
		super();
		Assert.notNull(expirationDate, "expirationDate cannot be null");
		Assert.notNull(token, "token cannot be null");
		this.expirationDate = expirationDate;
		this.token = token;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public String getToken() {
		return token;
	}
	
	public boolean hasExpired() {
		return expirationDate.before(new Date(System.currentTimeMillis() - expiredBefore));
	}
}
