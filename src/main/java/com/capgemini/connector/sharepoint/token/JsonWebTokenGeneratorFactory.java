package com.capgemini.connector.sharepoint.token;

import java.util.HashMap;
import java.util.Map;

import com.capgemini.connector.sharepoint.exception.SharePointFarmNotFoundException;


/**
 * Contains all Json Web Token Generators associated by SharePoint farm's
 * hostname
 * 
 * @author lhauspie
 *
 */
public class JsonWebTokenGeneratorFactory {

	private Map<String, JsonWebTokenGenerator> tokenGenerators = new HashMap<String, JsonWebTokenGenerator>();

	public JsonWebTokenGeneratorFactory(Map<String, JsonWebTokenGenerator> tokenGenerators) {
		super();
		this.tokenGenerators = tokenGenerators;
	}

	public JsonWebTokenGenerator getTokenGeneratorByHostname(String hostname) throws SharePointFarmNotFoundException {
		JsonWebTokenGenerator jsonWebTokenGenerator = tokenGenerators.get(hostname);
		if (null == jsonWebTokenGenerator) {
			throw new SharePointFarmNotFoundException(
					"Can't find the farm corresponding to the hostname \"" + hostname + "\"");
		}
		return jsonWebTokenGenerator;
	}
}
