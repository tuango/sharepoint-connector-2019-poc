package com.capgemini.connector.sharepoint.token;

import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.PrivateKey;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.util.Assert;

import io.jsonwebtoken.impl.TextCodec;

public class PfxFileAccessor {

private Logger log = LoggerFactory.getLogger(this.getClass());
	
	// from outside, passed in constructor parameters
	private String keyStoreType;
	private char[] keyStorePassword;
	private String keyAlias;
	private char[] keyPassword;

	// internal properties, initialized on Bean creation
	private KeyStore keyStore;
	private PrivateKey privateKey;
	private String thumbprint;

	// FIXME : better manage exception
	public PfxFileAccessor(String keyStoreType, Resource keyStoreResource, String keyStorePassword, String keyAlias,
			String keyPassword) {
		super();
		
		log.debug("keyStoreType : {}", keyStoreType);
		log.debug("keyStoreResource : {}", keyStoreResource);
		log.debug("keyStorePassword : {}", keyStorePassword);
		log.debug("keyAlias : {}", keyAlias);
		log.debug("keyPassword : {}", keyPassword);

		Assert.notNull(keyStoreType, "keyStoreType cannot be null");
		Assert.notNull(keyStoreResource, "keyStoreResource cannot be null");
		Assert.notNull(keyStorePassword, "keyStorePassword cannot be null");
		Assert.notNull(keyAlias, "keyAlias cannot be null");
		Assert.notNull(keyPassword, "keyPassword cannot be null");

		this.keyStoreType = keyStoreType;
		this.keyStorePassword = keyStorePassword.toCharArray();
		this.keyAlias = keyAlias;
		this.keyPassword = keyPassword.toCharArray();

		try {
			initKeyStore(keyStoreResource);
			initPrivateKey();
			initThumbprint();
		} catch (Exception e) {
			log.error("Error building the pfx File Accessor", e);
		}
	}

	// FIXME : better manage exception
	private void initKeyStore(Resource keyStoreResource) throws Exception {
		log.debug("initKeyStore({})", keyStoreResource);
		if (StringUtils.equals("PKCS12", keyStoreType)) {
			keyStore = KeyStore.getInstance(keyStoreType, "SunJSSE");
		} else {
			keyStore = KeyStore.getInstance(keyStoreType);
		}
		keyStore.load(keyStoreResource.getInputStream(), keyStorePassword);
	}

	// FIXME : better manage exception
	private void initPrivateKey() throws Exception {
		log.debug("initPrivateKey()");
		privateKey = (PrivateKey) keyStore.getKey(keyAlias, keyPassword);
	}

	// FIXME : better manage exception
	private void initThumbprint() throws Exception {
		log.debug("initThumbprint()");
		byte[] der = keyStore.getCertificate(keyAlias).getEncoded();
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		md.update(der);
		byte[] digest = md.digest();
		thumbprint = TextCodec.BASE64URL.encode(digest);
	}

	public PrivateKey getPrivateKey() {
		return privateKey;
	}

	public String getThumbprint() {
		return thumbprint;
	}
}

