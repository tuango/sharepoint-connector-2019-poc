package com.capgemini.connector.sharepoint.token;

import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Configuration;

import com.capgemini.connector.sharepoint.token.ReservedClaimConstants.Value.NameIdentifierIssuer;
import com.capgemini.connector.sharepoint.util.SharePointClientUtil;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Configuration
public class JsonWebTokenGenerator {

	// from outside
		private URI farmUri;
		private String urlAuthority;
		private String clientId;
		private String issuerId;
		private PfxFileAccessor pfxFileAccessor;
		private String sender;
		private int highTrustAccessTokenLifetime;
		private String realm;
		private String serviceAccountSid;
		private String trustedProvider;
		
		public JsonWebTokenGenerator(URI farmUri, String clientId, String issuerId, String sender,
				int highTrustAccessTokenLifetime, PfxFileAccessor pfxFileAccessor, String serviceAccountSid, String trustedProvider, String realm) {
			super();
			this.farmUri = farmUri;
			this.urlAuthority = farmUri.getAuthority();
			this.clientId = clientId;
			this.issuerId = issuerId;
			this.sender = sender;
			this.highTrustAccessTokenLifetime = highTrustAccessTokenLifetime;
			this.pfxFileAccessor = pfxFileAccessor;
			this.serviceAccountSid = serviceAccountSid;
			this.trustedProvider = trustedProvider;
			this.realm = realm;
		}

		// hard coded cache to add some additional behavior
		// like check if the token will expire in next X secondes.
		private Map<String, JsonWebToken> tokens = new HashMap<String, JsonWebToken>();

		public String getServiceAccountToken() {
			return getToken(serviceAccountSid);
		}
		
		public String getToken(String nameId) {
			if (StringUtils.isEmpty(realm)) {
				realm = SharePointClientUtil.getRealmFromTargetSite(farmUri);
			}
			
			String tokenKey = realm + "$" + nameId;
			if (!tokens.containsKey(tokenKey) || tokens.get(tokenKey).hasExpired()) {
				tokens.put(tokenKey, getS2SAccessToken(nameId));
			}
			return tokens.get(tokenKey).getToken();
		}

		private JsonWebToken getS2SAccessToken(String nameId) {
			Map<String, Object> claims = getClaimsWithWindowsIdentity(nameId);
			return issueToken(claims);
		}

		private Map<String, Object> getClaimsWithWindowsIdentity(String nameId) {
			Map<String, Object> claims = new HashMap<>();
			claims.put(ReservedClaimConstants.Key.NAME_IDENTIFIER, nameId.toLowerCase());
			String nameIdentifierIssuer = NameIdentifierIssuer.ACTIVE_DIRECTORY_PROVIDER;
			if (StringUtils.isNotBlank(trustedProvider)) {
				nameIdentifierIssuer = ReservedClaimConstants.Key.NAME_IDENTIFIER_ISSUER_PREFIX + trustedProvider.toLowerCase();
			}
			claims.put(ReservedClaimConstants.Key.NAME_IDENTIFIER_ISSUER, nameIdentifierIssuer);
			//claims.put(ReservedClaimConstants.Key.UPN, "tuan.ngo@capgemini.com");
			claims.put(ReservedClaimConstants.Key.UPN, "tuan.ngo@capgemini.com");//Service account has UPN is empty value
			return claims;
		}		

		private JsonWebToken issueToken(Map<String, Object> claims) {
			long nowMillis = System.currentTimeMillis();
			long expMillis = nowMillis + (highTrustAccessTokenLifetime * 60 * 60 * 1000);

			// ----actor token----
			String issuer = StringUtils.isEmpty(realm) ? issuerId : String.format("%s@%s", issuerId, realm);
			String nameid = StringUtils.isEmpty(realm) ? clientId : String.format("%s@%s", clientId, realm);
			String audience = String.format("%s/%s@%s", sender, urlAuthority, realm);

			Map<String, Object> actorClaims = new HashMap<>();
			actorClaims.put(ReservedClaimConstants.Key.NAME_IDENTIFIER, nameid);
			actorClaims.put(ReservedClaimConstants.Key.TRUSTED_FOR_IMPERSONATION_CLAIM_TYPE, Boolean.TRUE.toString());

			String actorTokenString = createActorTokenJWT(null, issuer, audience, nowMillis, expMillis, actorClaims);

			// ----out token----
			Map<String, Object> outerClaims = new HashMap<>();
			outerClaims.putAll(claims);
			outerClaims.put(ReservedClaimConstants.Key.ACTOR_TOKEN, actorTokenString);

			return createOutTokenJWT(null, nameid, audience, nowMillis, expMillis, outerClaims);
		}

		private String createActorTokenJWT(String id, String issuer, String audience, long strValidFrom, long strValidTo,
				Map<String, Object> claims) {
			/**
			 * ActorToken JWT Json sapmle {{ "typ":"JWT", "alg":"RS256",
			 * "x5t":"LRxHRIp-BKD7xG7-ktKmgoNT7Eo" }.{ "aud":
			 * "00000003-0000-0ff1-ce00-000000000000/sharepoint@200a8e79-a98e-4b79-a6e3-c637c6482471",
			 * "iss":
			 * "11111111-1111-1111-1111-111111111111@200a8e79-a98e-4b79-a6e3-c637c6482471",
			 * "nbf":"1444780800", "exp":"1444824000", "nameid":
			 * "4ebb8f86-b40c-4cc5-8255-4ebeea018dc5@200a8e79-a98e-4b79-a6e3-c637c6482471",
			 * "trustedfordelegation":"true" }}
			 */
			Map<String, Object> headers = new HashMap<>();
			headers.put(ReservedClaimConstants.Key.TYPE, ReservedClaimConstants.Value.JSON_WEB_TOKEN);
			headers.put(ReservedClaimConstants.Key.X5T, pfxFileAccessor.getThumbprint());

			claims.put(ReservedClaimConstants.Key.AUDIENCE, audience);
			claims.put(ReservedClaimConstants.Key.ISSUER, issuer);
			claims.put(ReservedClaimConstants.Key.NOT_BEFORE, String.valueOf(strValidFrom / 1000));
			claims.put(ReservedClaimConstants.Key.EXPIRATION, String.valueOf(strValidTo / 1000));
			JwtBuilder builder = Jwts.builder().setId(id).setClaims(claims).setHeader(headers)
					.signWith(SignatureAlgorithm.RS256, pfxFileAccessor.getPrivateKey());

			return builder.compact();
		}

		private JsonWebToken createOutTokenJWT(String id, String issuer, String audience, long strValidFrom,
				long strValidTo, Map<String, Object> claims) {
			/**
			 * Out Token JWT Json sapmle : {{ "typ":"JWT", "alg":"none" }.{ "aud":
			 * "00000003-0000-0ff1-ce00-000000000000/sharepoint@200a8e79-a98e-4b79-a6e3-c637c6482471",
			 * "iss":
			 * "4ebb8f86-b40c-4cc5-8255-4ebeea018dc5@200a8e79-a98e-4b79-a6e3-c637c6482471",
			 * "nbf":"1444889107", "exp":"1444932307",
			 * "nameid":"s-1-5-21-1030104071-1452137555-3129204420-500",
			 * "nii":"urn:office:idp:activedirectory", "actortoken":
			 * "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IkxSeEhSSXAtQktEN3hHNy1rdEttZ29OVDdFbyJ9.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTBmZjEtY2UwMC0wMDAwMDAwMDAwMDAvc2hhcmVwb2ludEAyMDBhOGU3OS1hOThlLTRiNzktYTZlMy1jNjM3YzY0ODI0NzEiLCJpc3MiOiIxMTExMTExMS0xMTExLTExMTEtMTExMS0xMTExMTExMTExMTFAMjAwYThlNzktYTk4ZS00Yjc5LWE2ZTMtYzYzN2M2NDgyNDcxIiwibmJmIjoiMTQ0NDc4MDgwMCIsImV4cCI6IjE0NDQ4MjQwMDAiLCJuYW1laWQiOiI0ZWJiOGY4Ni1iNDBjLTRjYzUtODI1NS00ZWJlZWEwMThkYzVAMjAwYThlNzktYTk4ZS00Yjc5LWE2ZTMtYzYzN2M2NDgyNDcxIiwidHJ1c3RlZGZvcmRlbGVnYXRpb24iOiJ0cnVlIn0.ug03mm3q6yinrqwT4MrwK-xRYTlND17NpzrNo4fjJNEVcsflcjmFMjFXAeaORCR-FNJrNnt5BMMRlTilwmOa9FnYqviA4GK-hKIkDFAs_GmmzidIBe72pX88dX375HO3bccLpVu_Q_9IcYD6j247PdRN0MgX2SJmrZ5BMoCEAcbwYqbGTyBgomSPs6rqgE5sTI5Pklk9p_gLKc-14PhkR9i-SAc9NwFSkBuun3GUxkMXOLkLN_pcN5wXlBvk6wumCC2VrAKXTevuSVp_qqGdSEWPKVxhbZtUYwNhq3WOCtZjroBsuUs4at4LpOTBjyH766ANg_DJWO2LGIXldpAGHA"
			 * }}
			 */
			Map<String, Object> headers = new HashMap<String, Object>();
			headers.put(ReservedClaimConstants.Key.TYPE, ReservedClaimConstants.Value.JSON_WEB_TOKEN);

			// x-ms-diagnostics: 3000003;
			// reason="Invalid audience Uri
			// '00000003-0000-0ff1-ce00-00000000/dev-ecollaborative.capgemini.com@e1836bf1-a142-40cb-8b35-4562751ce785'.";
			// category="invalid_client"
			claims.put(ReservedClaimConstants.Key.AUDIENCE, audience);
			claims.put(ReservedClaimConstants.Key.ISSUER, issuer);
			claims.put(ReservedClaimConstants.Key.NOT_BEFORE, String.valueOf(strValidFrom / 1000));
			claims.put(ReservedClaimConstants.Key.EXPIRATION, String.valueOf(strValidTo / 1000));

			JwtBuilder builder = Jwts.builder().setId(id).setHeader(headers).setClaims(claims);

			Date expirationDate = new Date(strValidTo);
			String token = builder.compact();
			return new JsonWebToken(expirationDate, token);
		}
}
