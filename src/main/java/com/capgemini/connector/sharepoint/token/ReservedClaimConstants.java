package com.capgemini.connector.sharepoint.token;

/**
 * @see <a href="https://docs.microsoft.com/en-us/sharepoint/dev/sp-add-ins/create-and-use-access-tokens-in-provider-hosted-high-trust-sharepoint-add-ins#structure-of-access-tokens">Structure JWT for SharePoint</a>
 * 
 * @author lhauspie
 */
public interface ReservedClaimConstants {
	
	public interface Key {
		public static final String AUDIENCE = "aud";
		public static final String ACTOR = "actor";
		public static final String ACTOR_TOKEN = "actortoken";
		public static final String APP_CONTEXT = "appctx";
		public static final String EXPIRATION = "exp";
		public static final String NAME_IDENTIFIER_ISSUER = "nii";//name identity issuer
		public static final String NAME_IDENTIFIER_ISSUER_PREFIX = "trusted:";
		public static final String ISSUED_AT = "iat";
		public static final String ISSUER = "iss";
		public static final String NAME_IDENTIFIER = "nameid";
		public static final String NOT_BEFORE = "nbf";
		public static final String TRUSTED_FOR_IMPERSONATION_CLAIM_TYPE = "trustedfordelegation";
		public static final String TYPE = "typ";
		public static final String X5T = "x5t";
		public static final String UPN = "upn";
	}

	public interface Value {
		public static final String JSON_WEB_TOKEN = "JWT";

		public interface Principal {
			public static final String ACS = "00000001-0000-0000-c000-000000000000"; //Azure Access Control (ACS),
			public static final String EXCHANGE = "00000002-0000-0ff1-ce00-000000000000";
			public static final String SHAREPOINT = "00000003-0000-0ff1-ce00-000000000000";
			public static final String LYNC = "00000004-0000-0ff1-ce00-000000000000";
			public static final String WORKFLOW = "00000005-0000-0000-c000-000000000000";
		}

		public interface NameIdentifierIssuer {
			public static final String ACTIVE_DIRECTORY_PROVIDER = "urn:office:idp:activedirectory";
			public static final String MEMBERSHIP_PROVIDER = "urn:office:idp:forms:membershipprovidername";
			public static final String SAML_PROVIDER = "trusted:samlprovidername";
		}
	}
}