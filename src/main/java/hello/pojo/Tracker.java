package hello.pojo;

import org.json.JSONObject;

public class Tracker extends SharepointJsonPojo {

	public static final String SP_TYPE = "SP.Data.TRACKER_x005f_LISTListItem";
	private static final String TRACKER_ID = "TrackerId";
	private static final String TRACKER_NAME = "TrackerName";
	private static final String TRACKER_UNIT = "TrackerUnit";
	
	public Tracker(JSONObject object) {
		super(object);
	}

	public Tracker() {
		super(new JSONObject());
	}
	
	public String getTrackerId() {
		return getString(TRACKER_ID);
	}

	public void setTrackerId(String value) {
		set(TRACKER_ID, value);
	}
	
	public String getTrackerName() {
		return getString(TRACKER_NAME);
	}
	
	public void setTrackerName(String value) {
		set(TRACKER_NAME, value);
	}
	
	public String getTrackerUnit() {
		return getString(TRACKER_UNIT);
	}
	
	public void setTrackerUnit(String value) {
		set(TRACKER_UNIT, value);
	}
}
