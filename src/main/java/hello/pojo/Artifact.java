package hello.pojo;

import org.json.JSONObject;

public class Artifact extends SharepointJsonPojo {

//	private static final String SP_TYPE = "SP.Data.${TrackerName}ListItem";
	private static final String ARTIFACT = "artifact";
	private static final String TITLE = "Title";
	private static final String PRIORITY = "priority";
	private static final String ESTIMATED_EFFORT = "estimatedEffort";
	private static final String ACTUAL_EFFORT = "actualEffort";
	private static final String REMAINING_EFFORT = "remainingEffort";
	private static final String POINTS = "points";
	private static final String ASSIGNED_TO_USERNAME_ID = "assignedToUsernameId";
	private static final String AUTHOR_ID = "AuthorId";
	private static final String EDITOR_ID = "EditorId";
	private static final String CATEGORY = "category";
	
	
	public Artifact(JSONObject object) {
		super(object);
	}

	public Artifact() {
		super(new JSONObject());
	}
	
	public String getArtifact() {
		return getString(ARTIFACT);
	}

	public void setArtifact(String value) {
		set(ARTIFACT, value);
	}

	public String getTitle() {
		return getString(TITLE);
	}
	
	public void setTitle(String value) {
		set(TITLE, value);
	}

	public String getPriority() {
		return getString(PRIORITY);
	}
	
	public void setPriority(String value) {
		set(PRIORITY, value);
	}
	
	public Double getEstimatedEffort() {
		return getDouble(ESTIMATED_EFFORT);
	}
	
	public void setEstimatedEffort(Double value) {
		set(ESTIMATED_EFFORT, value);
	}
	
	public Double getActualEffort() {
		return getDouble(ACTUAL_EFFORT);
	}
	
	public void setActualEffort(Double value) {
		set(ACTUAL_EFFORT, value);
	}
	
	public Double getRemainingEffort() {
		return getDouble(REMAINING_EFFORT);
	}
	
	public void setRemainingEffort(Double value) {
		set(REMAINING_EFFORT, value);
	}
	
	public Long getPoints() {
		return getLong(POINTS);
	}
	
	public void setPoints(Long value) {
		set(POINTS, value);
	}
	
	public Long getAssignedToUsernameId() {
		return getLong(ASSIGNED_TO_USERNAME_ID);
	}

	public void setAssignedToUsernameId(Long value) {
		set(ASSIGNED_TO_USERNAME_ID, value);
	}
	
	public Long getAuthorId() {
		return getLong(AUTHOR_ID);
	}
	
	public void setAuthorId(Long value) {
		set(AUTHOR_ID, value);
	}

	public Long getEditorId() {
		return getLong(EDITOR_ID);
	}
	
	public void setEditorId(Long value) {
		set(EDITOR_ID, value);
	}
	
	public String getCategory() {
		return getString(CATEGORY);
	}
	
	public void setCategory(String value) {
		set(CATEGORY, value);
	}
}
