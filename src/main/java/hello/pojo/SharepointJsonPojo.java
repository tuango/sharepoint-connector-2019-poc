package hello.pojo;

import org.json.JSONObject;

public abstract class SharepointJsonPojo extends AbstractJsonPojo {
	
	private static final String ID = "Id";
	private static final String GUID = "GUID";
	private static final String METADATA = "__metadata";
	
	public SharepointJsonPojo(JSONObject object) {
		super(object);
	}
	
	public Long getId() {
		return getLong(ID);
	}
	
	public String getGuid() {
		return getString(GUID);
	}
	
	public void setId(Long value) {
		set(ID, value);
	}
	
	public Metadata getMetadata() {
		return getObject(METADATA, Metadata.class);
	}
	
	public void setMetadata(Metadata metadata) {
		set(METADATA, metadata);
	}
}
