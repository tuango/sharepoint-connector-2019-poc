package hello.pojo;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public abstract class AbstractJsonPojo {
	
	private JSONObject object;
	
	public AbstractJsonPojo(JSONObject object) {
		this.object = object;
	}
	
	protected void set(String key, Object value) {
		if (value != null && value instanceof AbstractJsonPojo) {
			object.put(key, ((AbstractJsonPojo)value).getObject());
		} else {
			object.put(key, value);
		}
	}
	
	protected String getString(String key) {
		return object == null ? null : (object.isNull(key) ? null : object.getString(key));
	}
	
	protected Double getDouble(String key) {
		return object == null ? null : (object.isNull(key) ? null : object.getDouble(key));
	}
	
	protected Long getLong(String key) {
		return object == null ? null : (object.isNull(key) ? null : object.getLong(key));
	}
	
	protected <T extends AbstractJsonPojo> T getObject(String key, Class<T> clazz) {
		try {
			JSONObject internalObject = object == null ? null : (object.isNull(key) ? null : object.getJSONObject(key));
			return internalObject == null ? null : clazz.getConstructor(JSONObject.class).newInstance(internalObject);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	protected Collection<String> getStringCollection(String key) {
		List<String> supportedSchemaVersions = new ArrayList<>();
		JSONArray jsonArray = object.getJSONArray(key);
		for (int index = 0; index < jsonArray.length(); index++) {
			String value = jsonArray.getString(index);
			supportedSchemaVersions.add(value);
		}
		return Collections.unmodifiableCollection(supportedSchemaVersions);
		
	}
	
	protected void addStringToCollection(String key, String value) {
		object.accumulate(key, value);
	}
	
	protected void setStringCollection(String key, Collection<String> values) {
		object.put(key, new JSONArray(values));
	}
	
	protected JSONObject getObject() {
		return object;
	}
	
	public String toJson() {
		return object.toString();
	}
}
