package hello.pojo;

import org.json.JSONObject;

public class Metadata extends AbstractJsonPojo {

	private static final String TYPE = "type";
	private static final String ETAG = "etag";
	private static final String ID = "id";
	private static final String URI = "uri";
	
	public Metadata(JSONObject object) {
		super(object);
	}

	public Metadata() {
		super(new JSONObject());
	}
	
	public String getId() {
		return getString(ID);
	}

	public String getUri() {
		return getString(URI);
	}

	public String getType() {
		return getString(TYPE);
	}
	
	public void setType(String value) {
		set(TYPE, value);
	}
	
	public String getETag() {
		return getString(ETAG);
	}
}
