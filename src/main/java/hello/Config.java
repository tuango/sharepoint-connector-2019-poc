package hello;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.capgemini.connector.sharepoint.interceptor.LoggingClientHttpRequestInterceptor;
import com.capgemini.connector.sharepoint.interceptor.PerformanceClientHttpRequestInterceptor;

@Configuration
public class Config {
	@Bean
	public RestTemplate restTemplate(ClientHttpRequestFactory factory) throws Exception {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setRequestFactory(factory);
		restTemplate.setRequestFactory(new BufferingClientHttpRequestFactory(factory));
		restTemplate.getInterceptors().add(new PerformanceClientHttpRequestInterceptor());
		restTemplate.getInterceptors().add(new LoggingClientHttpRequestInterceptor());
		return restTemplate;
	}
	
	@Bean
	public ClientHttpRequestFactory clientHttpRequestFactory() {
		return new HttpComponentsClientHttpRequestFactory();
	}

}
