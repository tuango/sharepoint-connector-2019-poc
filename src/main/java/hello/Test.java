package hello;

import java.net.URI;

import org.json.JSONObject;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.capgemini.connector.sharepoint.interceptor.LoggingClientHttpRequestInterceptor;
import com.capgemini.connector.sharepoint.interceptor.PerformanceClientHttpRequestInterceptor;
import com.capgemini.connector.sharepoint.token.JsonWebTokenGenerator;
import com.capgemini.connector.sharepoint.token.PfxFileAccessor;
import com.capgemini.connector.sharepoint.token.ReservedClaimConstants;
import com.capgemini.connector.sharepoint.util.SharePointClientUtil;
import com.capgemini.connector.sharepoint.util.SharePointUrlMaker;

public class Test {

	public static void main(String[] args) {
		System.setProperty("http.proxyHost", "127.0.0.1");
		System.setProperty("http.proxyPort", "8888");
		System.setProperty("https.proxyHost", "127.0.0.1");
		System.setProperty("https.proxyPort", "8888");

//		try {
//	        // Build PfxFileAccessor
//	        String keyStoreType = "PKCS12";
//			Resource keyStore = new FileSystemResource("D:\\Projects\\My3D\\sources\\gs-consuming-rest-master\\initial\\src\\main\\resources\\eCollaborative_Subversion.pfx");
//			String keyStorePassword = "Collaborative@123";
//			String keyAlias = "63115993-36c7-4f6a-af81-1a209a4cd666";
//			String keyPassword = "Collaborative@123";
//			PfxFileAccessor pfxFileAccessor = new PfxFileAccessor(keyStoreType, keyStore, keyStorePassword, keyAlias, keyPassword);
//			
//			// Build TokenGenerator
//			String siteUriAuthority = "dev-ecollaborative.capgemini.com";
//			String clientId = "41994d46-9291-4e79-a467-4ca412e35354";
//			String issuerId = "a43bec08-05eb-491b-af11-43ab1089f35c";
//			int tokenLifetime = 12;
//			JsonWebTokenGenerator tokenGenerator = new JsonWebTokenGenerator(siteUriAuthority, clientId, issuerId, ReservedClaimConstants.Value.Principal.SHAREPOINT, tokenLifetime, pfxFileAccessor);
//			
//			// Build the RestTemplate to use
//			URI sharePointSiteUri = new URI("http://dev-ecollaborative.capgemini.com/sites/SiteColLogan/VVM_Tests");
//			String realm = SharePointClientUtil.getRealmFromTargetSite(sharePointSiteUri);
//			RestTemplate restTemplate = new RestTemplate();
//			restTemplate.setRequestFactory(new BufferingClientHttpRequestFactory(new HttpComponentsClientHttpRequestFactory()));
//			restTemplate.getInterceptors().add(new PerformanceClientHttpRequestInterceptor());
//			restTemplate.getInterceptors().add(new LoggingClientHttpRequestInterceptor());
//			
//			// Variable declaration
//			String token = tokenGenerator.getToken(realm, "S-1-5-21-1531082355-734649621-3782574898-1666960");
//			String urlToCall;
//			String trackerName = "Tracker100511";
//			JSONObject jsonObjectResult;
//			
//			// retrieve form digest to post some data after
//			urlToCall = sharePointSiteUri + SharePointUrlMaker.getContextInfo();
//			jsonObjectResult = SharePointClientUtil.postRequest(urlToCall, restTemplate, token);
//			String formDigest = jsonObjectResult.getJSONObject("d").getJSONObject("GetContextWebInformation").getString("FormDigestValue");
//
//			// BUSINESS CODE START HERE
//			urlToCall = sharePointSiteUri + SharePointUrlMaker.getListByTitleItemById(trackerName, 1);
//			
//			// retrieve the first artefact to update
//			System.out.println("retrieve the first artefact to update");
//			jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
//			String etag = jsonObjectResult.getJSONObject("d").getJSONObject("__metadata").getString("etag");
//			
//			// update the actual and remaining efforts
//			System.out.println("update the actual and remaining efforts");
//			JSONObject jsonObject = new JSONObject("{'__metadata':{'type':'SP.Data."+trackerName+"ListItem', 'etag':'" + etag + "'},'estimatedEffort':1.50,'actualEffort':1.25,'remainingEffort':0.25}");
//			jsonObjectResult = SharePointClientUtil.patchRequest(urlToCall, restTemplate, token, formDigest, jsonObject, etag);
//
//			
//			// retrieve the artefact just updated
//			System.out.println("retrieve the first artefact just updated");
//			jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
//        } catch (Exception e) {
//        	// TODO Auto-generated catch block
//        	e.printStackTrace();
//        }
    }
}
