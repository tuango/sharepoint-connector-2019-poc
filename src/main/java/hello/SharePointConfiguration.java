package hello;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.capgemini.connector.sharepoint.interceptor.LoggingClientHttpRequestInterceptor;
import com.capgemini.connector.sharepoint.interceptor.PerformanceClientHttpRequestInterceptor;
import com.capgemini.connector.sharepoint.token.JsonWebTokenGenerator;
import com.capgemini.connector.sharepoint.token.JsonWebTokenGeneratorFactory;
import com.capgemini.connector.sharepoint.token.PfxFileAccessor;
import com.capgemini.connector.sharepoint.token.ReservedClaimConstants;
import com.capgemini.connector.sharepoint.util.SharePointUrlInformationExtractor;

import hello.properties.ApplicationProperties;
import hello.properties.SharePointPfxFileAccessorProperties;
import hello.properties.SharePointProperties;
import hello.properties.SharePointServerProperties;
import hello.properties.SharePointTokenProperties;
import lombok.Data;

/**
 * 	The Spring Java based configuration about SharePoint<br/>
	Contains a Java based representation of the properties file (sharePointProperties property).<br/>
	Allows to have an easy access to the properties of one farm by using Java
	Initialises the JsonWebTokenGeneratorFactory Bean with the SharePoint configuration with the aid of serverPropertiesMap.
	We had to code an afterPropertiesSet method to loop over the properties to build the JsonWebTokenGeneratorFactory
@author lhauspie
 *
 */
@Data
@Configuration
public class SharePointConfiguration {

	private final String SERVER_PREFIX = "sharepoint.servers.";
	private final String SUFFIX_HOSTNAME = ".hostname";
	private final String SUFFIX_FARM_URL = ".farm-url";
	private final String SUFFIX_URL_EXTRACTOR_REGEX = ".url-extractor-regex";
	private final String SUFFIX_KEYSTORE_TYPE = ".pfx-file-accessor.keystore-type";
	private final String SUFFIX_KEYSTORE_PATH = ".pfx-file-accessor.keystore-path";
	private final String SUFFIX_KEYSTORE_PSWD = ".pfx-file-accessor.keystore-password";
	private final String SUFFIX_KEY_ALIAS = ".pfx-file-accessor.key-alias";
	private final String SUFFIX_KEY_PSWD = ".pfx-file-accessor.key-password";
	private final String SUFFIX_TOKEN_LIFETIME = ".token.lifetime";
	private final String SUFFIX_TOKEN_ISSUER_ID = ".token.issuer-id";
	private final String SUFFIX_TOKEN_CLIENT_ID = ".token.client-id";
	private final String SUFFIX_SERVICE_ACCOUNT_SID = ".service-account-sid";
	private final String SUFFIX_TRUSTED_PROVIDER = ".trusted-provider";
	private final String SUFFIX_TRUSTED_REALMID = ".realmId";

	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private ApplicationProperties applicationProperty;

	
	@Bean
	public JsonWebTokenGeneratorFactory getSharePointTokenGeneratorFactory() {
		Map<String, String> properties = applicationProperty.getProperties();
		Map<String, JsonWebTokenGenerator> generators = new HashMap<String, JsonWebTokenGenerator>();
		int i = 0;
		while (properties.containsKey(SERVER_PREFIX + i + SUFFIX_HOSTNAME)) {
			String hostname = (String) properties.get(SERVER_PREFIX + i + SUFFIX_HOSTNAME);
			String keystoreType = (String) properties.get(SERVER_PREFIX + i + SUFFIX_KEYSTORE_TYPE);
			String keystorePath = (String) properties.get(SERVER_PREFIX + i + SUFFIX_KEYSTORE_PATH);
			String keystorePassword = (String) properties.get(SERVER_PREFIX + i + SUFFIX_KEYSTORE_PSWD);
			String keyAlias = (String) properties.get(SERVER_PREFIX + i + SUFFIX_KEY_ALIAS);
			String keyPassword = (String) properties.get(SERVER_PREFIX + i + SUFFIX_KEY_PSWD);
			int tokenLifetime = Integer.parseInt((String) properties.get(SERVER_PREFIX + i + SUFFIX_TOKEN_LIFETIME));
			String tokenIssuerId = (String) properties.get(SERVER_PREFIX + i + SUFFIX_TOKEN_ISSUER_ID);
			String tokenClientId = (String) properties.get(SERVER_PREFIX + i + SUFFIX_TOKEN_CLIENT_ID);
			String farmUrl = (String) properties.get(SERVER_PREFIX + i + SUFFIX_FARM_URL);
			String serviceAccountSid = (String) properties.get(SERVER_PREFIX + i + SUFFIX_SERVICE_ACCOUNT_SID);
			String trustedProvider = (String) properties.get(SERVER_PREFIX + i + SUFFIX_TRUSTED_PROVIDER);
			String realm = (String) properties.get(SERVER_PREFIX + i + SUFFIX_TRUSTED_REALMID);

			URI farmUri;
			try {
				farmUri = new URI(farmUrl);
			} catch (URISyntaxException e) {
				// FIXME manage better the exception
				throw new RuntimeException(e);
			}

			PfxFileAccessor pfxFileAccessor = new PfxFileAccessor(keystoreType,
					new FileSystemResource(new File(keystorePath)), keystorePassword, keyAlias, keyPassword);
			JsonWebTokenGenerator generator = new JsonWebTokenGenerator(farmUri, tokenClientId, tokenIssuerId,
					ReservedClaimConstants.Value.Principal.SHAREPOINT, tokenLifetime, pfxFileAccessor, serviceAccountSid, trustedProvider, realm);
			generators.put(hostname, generator);

			i++;
		}

		return new JsonWebTokenGeneratorFactory(generators);
	}

	@Bean
	public SharePointUrlInformationExtractor getSharePointUrlInformationExtractor() {
		Map<String, String> properties = applicationProperty.getProperties();
		Map<String, String> regularExpressions = new HashMap<String, String>();
		int i = 0;
		while (properties.containsKey(SERVER_PREFIX + i + SUFFIX_HOSTNAME)) {
			String urlExtractorRegex = (String) properties.get(SERVER_PREFIX + i + SUFFIX_URL_EXTRACTOR_REGEX);
			String hostname = (String) properties.get(SERVER_PREFIX + i + SUFFIX_HOSTNAME);
			regularExpressions.put(hostname, urlExtractorRegex);
			i++;
		}
		
		return new SharePointUrlInformationExtractor(regularExpressions);
	}
	
	@Bean
	public RestTemplate getRestTemplate(final ClientHttpRequestFactory clientHttpRequestFactory) {
		final RestTemplate restTemplate = new RestTemplate();
		restTemplate.setRequestFactory(clientHttpRequestFactory);
		if (true) {
			restTemplate.setRequestFactory(new BufferingClientHttpRequestFactory(clientHttpRequestFactory));
			restTemplate.getInterceptors().add(new LoggingClientHttpRequestInterceptor());
		}
		if (true) {
			restTemplate.getInterceptors().add(new PerformanceClientHttpRequestInterceptor());
		}
		return restTemplate;
	}

	@Bean
	public ClientHttpRequestFactory clientHttpRequestFactory(final HttpClient httpClient) {
		return new HttpComponentsClientHttpRequestFactory(httpClient);
	}

	@Bean
	public HttpClient httpClient() throws Exception {
		return HttpClients.createDefault();
	}
	
	@Bean
	public SharePointProperties getSharePointProperties() {
		SharePointProperties sharePointProperties = new SharePointProperties();
		Map<String, String> properties = applicationProperty.getProperties();
		int i = 0;
		while (properties.containsKey(SERVER_PREFIX + i + SUFFIX_HOSTNAME)) {
			String hostname = (String) properties.get(SERVER_PREFIX + i + SUFFIX_HOSTNAME);
			String keystoreType = (String) properties.get(SERVER_PREFIX + i + SUFFIX_KEYSTORE_TYPE);
			String keystorePath = (String) properties.get(SERVER_PREFIX + i + SUFFIX_KEYSTORE_PATH);
			String keystorePassword = (String) properties.get(SERVER_PREFIX + i + SUFFIX_KEYSTORE_PSWD);
			String keyAlias = (String) properties.get(SERVER_PREFIX + i + SUFFIX_KEY_ALIAS);
			String keyPassword = (String) properties.get(SERVER_PREFIX + i + SUFFIX_KEY_PSWD);
			int tokenLifetime = Integer.parseInt((String) properties.get(SERVER_PREFIX + i + SUFFIX_TOKEN_LIFETIME));
			String tokenIssuerId = (String) properties.get(SERVER_PREFIX + i + SUFFIX_TOKEN_ISSUER_ID);
			String tokenClientId = (String) properties.get(SERVER_PREFIX + i + SUFFIX_TOKEN_CLIENT_ID);
			String serviceAccountSID = (String) properties.get(SERVER_PREFIX + i + SUFFIX_SERVICE_ACCOUNT_SID);
			String trustedProvider = (String) properties.get(SERVER_PREFIX + i + SUFFIX_TRUSTED_PROVIDER);

			SharePointServerProperties server = new SharePointServerProperties();
			server.setHostname(hostname);
			server.setServiceAccountSID(serviceAccountSID);
			server.setTrustedProvider(trustedProvider);

			SharePointPfxFileAccessorProperties pfxFileAccessorProperties = new SharePointPfxFileAccessorProperties();
			pfxFileAccessorProperties.setKeystoreType(keystoreType);
			pfxFileAccessorProperties.setKeystorePath(keystorePath);
			pfxFileAccessorProperties.setKeystorePassword(keystorePassword);
			pfxFileAccessorProperties.setKeyAlias(keyAlias);
			pfxFileAccessorProperties.setKeyPassword(keyPassword);
			server.setPfxFileAccessor(pfxFileAccessorProperties);

			SharePointTokenProperties token = new SharePointTokenProperties();
			token.setLifetime(tokenLifetime);
			token.setClientId(tokenClientId);
			token.setIssuerId(tokenIssuerId);
			server.setToken(token);

			sharePointProperties.put(server.getHostname(), server);
			i++;
		}
		
		return sharePointProperties;
	}
}
