/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 my3D team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package hello.rest;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.connector.sharepoint.token.JsonWebTokenGenerator;
import com.capgemini.connector.sharepoint.token.PfxFileAccessor;
import com.capgemini.connector.sharepoint.token.ReservedClaimConstants;
import com.capgemini.connector.sharepoint.util.SharePointClientUtil;

import hello.SharePointConfiguration;

/**
 * <p>
 * Endpoints of the REST API relying on sharepoint.
 * </p>
 *
 * @author lhauspie
 */
@RestController
public class ApplicationService implements InitializingBean {

	private static Logger LOGGER = Logger.getLogger(ApplicationService.class);

	@Autowired
	private SharePointConfiguration appConfig;
	
	private JsonWebTokenGenerator tokenGenerator;
	private String realm;
	
	public ApplicationService() throws Exception {
		LOGGER.info("instanciating ApplicationService");
	}
	
	/**
	 * <p>
	 * Generates an access token.
	 * </p>
	 *
	 * @param request
	 *            the sharepoint endpoint parameters
	 * @return the access token
	 * @throws IOException
	 *             if ACS can't be reached
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/token")
	public String token(@RequestParam final String nameId) throws IOException {
		try {
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		LOGGER.info("token");
		return tokenGenerator.getToken(nameId);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
//		LOGGER.info("initVariables appConfig not null");
//		PfxFileAccessor pfxFileAccessor = new PfxFileAccessor(appConfig.getKeyStoreType(), appConfig.getKeyStore(), appConfig.getKeyStorePassword(), appConfig.getKeyAlias(), appConfig.getKeyPassword());
//		tokenGenerator = new JsonWebTokenGenerator(appConfig.getSiteUri().getAuthority(), appConfig.getClientId(), appConfig.getIssuerId(), ReservedClaimConstants.Value.Principal.SHAREPOINT, appConfig.getTokenLifetime(), pfxFileAccessor);
//		realm = SharePointClientUtil.getRealmFromTargetSite(appConfig.getSiteUri());
	}
}
