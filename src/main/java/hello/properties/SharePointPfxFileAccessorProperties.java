package hello.properties;

public class SharePointPfxFileAccessorProperties {
	private String keystoreType;
	private String keystorePath;
	private String keystorePassword;
	private String keyAlias;
	private String keyPassword;
	
	@Override
	public String toString() {
		return "SharePointPfxFileAccessorProperties [keystoreType=" + keystoreType + ", keystorePath=" + keystorePath
				+ ", keystorePassword=" + keystorePassword + ", keyAlias=" + keyAlias + ", keyPassword=" + keyPassword
				+ "]";
	}

	public String getKeystoreType() {
		return keystoreType;
	}

	public void setKeystoreType(String keystoreType) {
		this.keystoreType = keystoreType;
	}

	public String getKeystorePath() {
		return keystorePath;
	}

	public void setKeystorePath(String keystorePath) {
		this.keystorePath = keystorePath;
	}

	public String getKeystorePassword() {
		return keystorePassword;
	}

	public void setKeystorePassword(String keystorePassword) {
		this.keystorePassword = keystorePassword;
	}

	public String getKeyAlias() {
		return keyAlias;
	}

	public void setKeyAlias(String keyAlias) {
		this.keyAlias = keyAlias;
	}

	public String getKeyPassword() {
		return keyPassword;
	}

	public void setKeyPassword(String keyPassword) {
		this.keyPassword = keyPassword;
	}
	
}
