package hello.properties;

import lombok.Data;

@Data
public class SharePointTokenProperties {
	private int lifetime;
	private String issuerId;
	private String clientId;

	@Override
	public String toString() {
		return "SharePointTokenProperties [lifetime=" + lifetime + ", issuerId=" + issuerId + ", clientId=" + clientId
				+ "]";
	}
}
