package hello.properties;

public class SharePointServiceAccountProperties {
	private String sid;

	@Override
	public String toString() {
		return "SharePointServiceAccountProperties [sid=" + sid + "]";
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}
}
