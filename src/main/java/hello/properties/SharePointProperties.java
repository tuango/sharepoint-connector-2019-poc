package hello.properties;

import java.util.HashMap;

/**
 * The java representation of the properties file content concerning the
 * "SharePoint.xxx".<br/>
 * The Key of this map is the hostname of the farm.<br/>
 * The Value of this map is the properties content associated with the farm
 * (SharePointServerProperties)<br/>
 * Built by {@link com.capgemini.vvm.core.configuration.SharePointConfiguration}
 * 
 * @author lhauspie
 *
 */
public class SharePointProperties extends HashMap<String, SharePointServerProperties> {

}
