package hello.properties;


import lombok.Data;

@Data
public class SharePointServerProperties {
	private String hostname;
	private SharePointPfxFileAccessorProperties pfxFileAccessor;
	private SharePointTokenProperties token;
	private String serviceAccountSID;
	private String trustedProvider;
	@Override
	public String toString() {
		return "SharePointServerProperties [hostname=" + hostname + ", pfxFileAccessor=" + pfxFileAccessor + ", token="
				+ token + ", serviceAccountSID=" + serviceAccountSID + ", trustedProvider=" + trustedProvider + "]";
	}
}
