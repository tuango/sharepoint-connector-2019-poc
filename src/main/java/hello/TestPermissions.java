package hello;

import java.util.HashMap;
import java.util.Map;

public class TestPermissions {

	private static Map<String, Integer> permissions = new HashMap<String, Integer>();
	static {
		permissions.put("ViewListItems", Integer.valueOf(1));
		permissions.put("AddListItems", Integer.valueOf(2));
		permissions.put("EditListItems", Integer.valueOf(3));
		permissions.put("DeleteListItems", Integer.valueOf(4));
		permissions.put("ApproveItems", Integer.valueOf(5));
		permissions.put("OpenItems", Integer.valueOf(6));
		permissions.put("ViewVersions", Integer.valueOf(7));
		permissions.put("DeleteVersions", Integer.valueOf(8));
		permissions.put("CancelCheckout", Integer.valueOf(9));
		permissions.put("ManagePersonalViews", Integer.valueOf(10));
		permissions.put("ManageLists", Integer.valueOf(12));
		permissions.put("ViewFormPages", Integer.valueOf(13));
		permissions.put("AnonymousSearchAccessList", Integer.valueOf(14));
		permissions.put("Open", Integer.valueOf(17));
		permissions.put("ViewPages", Integer.valueOf(18));
		permissions.put("AddAndCustomizePages", Integer.valueOf(19));
		permissions.put("ApplyThemeAndBorder", Integer.valueOf(20));
		permissions.put("ApplyStyleSheets", Integer.valueOf(21));
		permissions.put("ViewUsageData", Integer.valueOf(22));
		permissions.put("CreateSSCSite", Integer.valueOf(23));
		permissions.put("ManageSubwebs", Integer.valueOf(24));
		permissions.put("CreateGroups", Integer.valueOf(25));
		permissions.put("ManagePermissions", Integer.valueOf(26));
		permissions.put("BrowseDirectories", Integer.valueOf(27));
		permissions.put("BrowseUserInfo", Integer.valueOf(28));
		permissions.put("AddDelPrivateWebParts", Integer.valueOf(29));
		permissions.put("UpdatePersonalWebParts", Integer.valueOf(30));
		permissions.put("ManageWeb", Integer.valueOf(31));
		permissions.put("AnonymousSearchAccessWebLists", Integer.valueOf(32));
		permissions.put("UseClientIntegration", Integer.valueOf(37));
		permissions.put("UseRemoteAPIs", Integer.valueOf(38));
		permissions.put("ManageAlerts", Integer.valueOf(39));
		permissions.put("CreateAlerts", Integer.valueOf(40));
		permissions.put("EditMyUserInfo", Integer.valueOf(41));
		permissions.put("EnumeratePermissions", Integer.valueOf(63));
	}

	public static void main(String[] args) {
		for (Map.Entry<String, Integer> permission : permissions.entrySet()) {
			if (hasPermission(2147483647l, 4294967295l, permission.getValue().intValue())) {
				System.out.println("user has the " + permission.getKey() + " permission !");
			}else {
				System.out.println("user doesn't have the " + permission.getKey() + " permission !");
			}
		}
	}

	public static boolean hasPermission(long low, long high, int bitIndex) {
		long sequence = low;
		if (bitIndex >= 32) {
			sequence = high;
			bitIndex -= 32;
		}
		return ((2 ^ (bitIndex - 1)) | sequence) == sequence;
	}
}
