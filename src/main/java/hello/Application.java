package hello;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.client.ClientProtocolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.capgemini.connector.sharepoint.interceptor.LoggingClientHttpRequestInterceptor;
import com.capgemini.connector.sharepoint.interceptor.PerformanceClientHttpRequestInterceptor;
import com.capgemini.connector.sharepoint.token.JsonWebTokenGenerator;
import com.capgemini.connector.sharepoint.token.JsonWebTokenGeneratorFactory;
import com.capgemini.connector.sharepoint.token.PfxFileAccessor;
import com.capgemini.connector.sharepoint.token.ReservedClaimConstants;
import com.capgemini.connector.sharepoint.util.SharePointClientUtil;

import hello.properties.ApplicationProperties;
import hello.rest.GetFedAuthCookieHttpClient;
import hello.rest.GetHistoryDataArtifact;
import hello.usecase.sanity.ArtifactSanityCheck;
import hello.usecase.sanity.UsersSanityCheck;

@SpringBootApplication
public class Application implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	@Autowired
	private SharePointConfiguration appConfig; // OK
	
	@Autowired
	private JsonWebTokenGeneratorFactory tokenGeneratorFactory;
	
	@Autowired
	private RestTemplate restTemplate;	

	
	public static void main(String args[]) {
		SpringApplication.run(Application.class);
	}


//	private void initSomeData() throws Exception {
//		PfxFileAccessor pfxFileAccessor = new PfxFileAccessor(appConfig.getKeyStoreType(), appConfig.getKeyStore(), appConfig.getKeyStorePassword(), appConfig.getKeyAlias(), appConfig.getKeyPassword());
//		tokenGenerator = new JsonWebTokenGenerator(appConfig.getSiteUri().getAuthority(), appConfig.getClientId(), appConfig.getIssuerId(), ReservedClaimConstants.Value.Principal.SHAREPOINT, appConfig.getTokenLifetime(), pfxFileAccessor);
//		realm = SharePointClientUtil.getRealmFromTargetSite(appConfig.getSiteUri());
//	}

	@Override
	public void run(String... args) throws Exception {
		
		//======================================TESTING GET HISTORY DATA============================
		//testGetHistory();
		//==========================================================================================
		
		//initSomeData();
		//nextGen-preprod
//	JsonWebTokenGenerator jsonWebTokenGenerator = tokenGeneratorFactory.getTokenGeneratorByHostname("ng-preprod.capgemini.com");
//	jsonWebTokenGenerator.getServiceAccountToken();
//	String siteUrl = "https://ng-preprod.capgemini.com/sites/1900_Referencestep66_mig_nextGen/step66";//https://ng-dev.capgemini.com/sites/ngevvmtest/project_test/_api/lists/getByTitle('tracker_tuango_test')/items
//	URI sharePointSiteUri = new URI(siteUrl);
//	String serviceAccount = "svc-gr-vvm";
//	String trackerId = "tracker0761524";
	
	
	
	JsonWebTokenGenerator jsonWebTokenGenerator = tokenGeneratorFactory.getTokenGeneratorByHostname("ng-rec.capgemini.com");
	jsonWebTokenGenerator.getServiceAccountToken();
	String siteUrl = "https://ng-rec.capgemini.com/sites/1900_Referencestep66_mig3_nextGen/step66";//https://ng-dev.capgemini.com/sites/ngevvmtest/project_test/_api/lists/getByTitle('tracker_tuango_test')/items
	URI sharePointSiteUri = new URI(siteUrl);
	String serviceAccount = "kpivvm";
	String trackerId = "tracker0762499";
	
	//https://ng-preprod.capgemini.com/sites/1900_Referencestep66_mig_nextGen/step66/Lists/tracker0761524/TrackerHomeForm.aspx
		
		
	// Start: SharePoint 2019	
//	JsonWebTokenGenerator jsonWebTokenGenerator = tokenGeneratorFactory.getTokenGeneratorByHostname("ngp-lab-adfs.cgtest.net");
//	jsonWebTokenGenerator.getServiceAccountToken();
//	String siteUrl = "https://ngp-lab-adfs.cgtest.net/sites/Nextgenstep66V1802";
//	URI sharePointSiteUri = new URI(siteUrl);
//	String serviceAccount = "svc-fr-grtestvvm2";
//	String trackerId = "tracker94416";
	// End: SharePoint 2019
		
	log.info("BEGIN");
	new ArtifactSanityCheck(restTemplate, sharePointSiteUri, jsonWebTokenGenerator, serviceAccount, trackerId).run();
	//new UsersSanityCheck(restTemplate, sharePointSiteUri, jsonWebTokenGenerator, serviceAccount, null).run();
	//RestTemplate restTemplate, URI sharePointSiteUri, String realm,
	
	//JsonWebTokenGenerator tokenGenerator, String nameId
	//appConfig.getSharePointProperties().displayServers();
	//new UsersSanityCheck(restTemplate, sharePointSiteUri, jsonWebTokenGenerator, serviceAccount, trackerId).run();
//				new TrackersSanityCheck(restTemplate, appConfig.getSiteUri(), realm, tokenGenerator, appConfig.getServiceAccountSID()).run();
//				new CreateSharePointPlanner(restTemplate, appConfig.getSiteUri(), realm, tokenGenerator, appConfig.getServiceAccountSID(), "Tracker100511").run();
//				new Daily(restTemplate, appConfig.getSiteUri(), realm, tokenGenerator, appConfig.getServiceAccountSID(), "Tracker100511").run();
	log.info("END");
		
	}
	
	private void testGetHistory() throws RestClientException, URISyntaxException, ClientProtocolException, IOException {
		
		//new GetHistoryDataArtifact().getFedAuthTokenFromCookieFollowRedirect(restTemplate);
		//new GetHistoryDataArtifact().getFedAuthTokenFromCookie(restTemplate);
		//new GetHistoryDataArtifact().getSAMLTokenXml(restTemplate);
		new GetFedAuthCookieHttpClient().getFedAuthCookie();
	}
	
	
}