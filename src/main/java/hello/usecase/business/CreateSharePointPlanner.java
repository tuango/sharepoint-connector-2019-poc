package hello.usecase.business;

import java.net.URI;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.client.RestTemplate;

import com.capgemini.connector.sharepoint.exception.SharePointClientException;
import com.capgemini.connector.sharepoint.token.JsonWebTokenGenerator;
import com.capgemini.connector.sharepoint.util.SharePointClientUtil;
import com.capgemini.connector.sharepoint.util.SharePointODataOperator;
import com.capgemini.connector.sharepoint.util.SharePointUrlMaker;

import hello.usecase.IUseCase;

public class CreateSharePointPlanner implements IUseCase {

	private static final Logger log = Logger.getLogger(CreateSharePointPlanner.class);

	private RestTemplate restTemplate;
	private URI sharePointSiteUri;
	private String realm;
	private JsonWebTokenGenerator tokenGenerator;
	private String nameId;
	private String trackerName;

	public CreateSharePointPlanner(RestTemplate restTemplate, URI sharePointSiteUri, String realm,
			JsonWebTokenGenerator tokenGenerator, String nameId, String trackerName) {
		super();
		this.restTemplate = restTemplate;
		this.sharePointSiteUri = sharePointSiteUri;
		this.realm = realm;
		this.tokenGenerator = tokenGenerator;
		this.nameId = nameId;
		this.trackerName = trackerName;
	}
	
	@Override
	public void run() throws JSONException, SharePointClientException {
		String urlToCall;
		String token = tokenGenerator.getToken(nameId);
		JSONObject jsonObjectResult;
		SharePointODataOperator oDataOperator;
		
		log.info("retrieve all trackers");
		// retrieve all trackers
		urlToCall = sharePointSiteUri + SharePointUrlMaker.getListByTitleItems("TRACKER_LIST");
		jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
		
		log.info("retrieve all users can access to the chosen tracker");
		// retrieve all users can access to the chosen tracker
		oDataOperator = new SharePointODataOperator("Member/Title,Member/UserId,Member/Email,Member/Users/Title,Member/Users/UserId,Member/Users/Email", null, "member/users", null);
		urlToCall = sharePointSiteUri + SharePointUrlMaker.getListByTitleRoleAssignments(trackerName, oDataOperator);
		jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
		
		log.info("retrieve workflow steps");
		// retrieve workflow steps
		oDataOperator = new SharePointODataOperator(null, "InternalName eq 'status'", null, null);
		urlToCall = sharePointSiteUri + SharePointUrlMaker.getListByTitleFields(trackerName, oDataOperator);
		jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
		
		log.info("retrieve priorities");
		// retrieve priorities
		oDataOperator = new SharePointODataOperator(null, "InternalName eq 'priority'", null, null);
		urlToCall = sharePointSiteUri + SharePointUrlMaker.getListByTitleFields(trackerName, oDataOperator);
		jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
		
		log.info("retrieve categories");
		// retrieve categories
		oDataOperator = new SharePointODataOperator(null, "InternalName eq 'category'", null, null);
		urlToCall = sharePointSiteUri + SharePointUrlMaker.getListByTitleFields(trackerName, oDataOperator);
		jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
		
		log.info("retrieve all choices fields");
		// retrieve all choices fields
		oDataOperator = new SharePointODataOperator(null, "typeAsString eq 'Choice'", null, null);
		urlToCall = sharePointSiteUri + SharePointUrlMaker.getListByTitleFields(trackerName, oDataOperator);
		jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
		
		log.info("retrieve all fields of the chosen tracker and their types");
		// retrieve all fields of the chosen tracker and their types
		urlToCall = sharePointSiteUri + SharePointUrlMaker.getListByTitleFields(trackerName);
		jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
		
		log.info("retrieve all artefacts of the chosen tracker");
		// retrieve all artefacts of the chosen tracker
		urlToCall = sharePointSiteUri + SharePointUrlMaker.getListByTitleItems(trackerName);
		jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
	}
}
