package hello.usecase.business;

import java.net.URI;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.client.RestTemplate;

import com.capgemini.connector.sharepoint.exception.SharePointClientException;
import com.capgemini.connector.sharepoint.token.JsonWebTokenGenerator;
import com.capgemini.connector.sharepoint.util.SharePointClientUtil;
import com.capgemini.connector.sharepoint.util.SharePointUrlMaker;

import hello.usecase.IUseCase;

public class Daily implements IUseCase {

	private static final Logger log = Logger.getLogger(Daily.class);

	private RestTemplate restTemplate;
	private URI sharePointSiteUri;
	private String realm;
	private JsonWebTokenGenerator tokenGenerator;
	private String nameId;
	private String trackerName;

	public Daily(RestTemplate restTemplate, URI sharePointSiteUri, String realm,
			JsonWebTokenGenerator tokenGenerator, String nameId, String trackerName) {
		super();
		this.restTemplate = restTemplate;
		this.sharePointSiteUri = sharePointSiteUri;
		this.realm = realm;
		this.tokenGenerator = tokenGenerator;
		this.nameId = nameId;
		this.trackerName = trackerName;
	}

	@Override
	public void run() throws JSONException, SharePointClientException {
		String urlToCall;
		String token = tokenGenerator.getToken(nameId);
		JSONObject jsonObject;
		JSONObject jsonObjectResult;
		String etag;
		
		log.info("retrieve form digest to post some data after");
		// retrieve form digest to post some data after
		urlToCall = sharePointSiteUri + SharePointUrlMaker.getContextInfo();
		jsonObjectResult = SharePointClientUtil.postRequest(urlToCall, restTemplate, token);
		String formDigest = jsonObjectResult.getJSONObject("d").getJSONObject("GetContextWebInformation").getString("FormDigestValue");
		
		log.info("retrieve all the items of the tracker to display the board");
		// retrieve all the items of the tracker to display the board
		urlToCall = sharePointSiteUri + SharePointUrlMaker.getListByTitleItems(trackerName);
		jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);

		log.info("create a StickyNote");
		// create a StickyNote
		urlToCall = sharePointSiteUri + SharePointUrlMaker.getListByTitleItems(trackerName);
		jsonObject = new JSONObject(generateStickyNote(trackerName, "Title 1", "Description 1", "10", "0", "10", "Architecture 1", "1", "1"));
		jsonObjectResult = SharePointClientUtil.postRequest(urlToCall, restTemplate, token, formDigest, jsonObject, null);
		int newStickyNoteIndex = jsonObjectResult.getJSONObject("d").getInt("ID");
		
		
		
		urlToCall = sharePointSiteUri + SharePointUrlMaker.getListByTitleItemById(trackerName, 1);
		log.info("retrieve the first artefact to update");
		// retrieve the artefact to update
		jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
		etag = jsonObjectResult.getJSONObject("d").getJSONObject("__metadata").getString("etag");
		int ndETag = Integer.parseInt(etag.replaceAll("\"", ""));
		etag = "\"" + (ndETag - 1) +"\"";
		
		log.info("update the actual and remaining efforts");
		// update the actual and remaining efforts
		jsonObject = new JSONObject("{'__metadata':{'type':'SP.Data."+trackerName+"ListItem', 'etag':'" + etag + "'},'actualEffort':0.25,'remainingEffort':1.75}");
		jsonObjectResult = SharePointClientUtil.patchRequest(urlToCall, restTemplate, token, formDigest, jsonObject, etag);

		log.info("retrieve the first artefact just updated");
		// retrieve the artefact just updated
		jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
		
		
		
		urlToCall = sharePointSiteUri + SharePointUrlMaker.getListByTitleItemById(trackerName, 2);
		log.info("retrieve the second artefact to update");
		// retrieve the artefact to update
		jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
		etag = jsonObjectResult.getJSONObject("d").getJSONObject("__metadata").getString("etag");

		log.info("update the actual and remaining efforts");
		// update the priority
		jsonObject = new JSONObject("{'__metadata':{'type':'SP.Data."+trackerName+"ListItem', 'etag':'" + etag + "'},'priority':'1'}");
		jsonObjectResult = SharePointClientUtil.patchRequest(urlToCall, restTemplate, token, formDigest, jsonObject, etag);
		
		log.info("retrieve the second artefact just updated");
		// retrieve the artefact just updated
		jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
		
		
		
		urlToCall = sharePointSiteUri + SharePointUrlMaker.getListByTitleItemById(trackerName, 3);
		log.info("retrieve the third artefact to update");
		// retrieve the artefact just updated
		jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
		etag = jsonObjectResult.getJSONObject("d").getJSONObject("__metadata").getString("etag");
		
		log.info("update the actual and remaining efforts");
		// update the category
		jsonObject = new JSONObject("{'__metadata':{'type':'SP.Data."+trackerName+"ListItem', 'etag':'" + etag + "'},'category':'Documentation'}");
		jsonObjectResult = SharePointClientUtil.patchRequest(urlToCall, restTemplate, token, formDigest, jsonObject, etag);
		
		log.info("retrieve the third artefact just updated");
		// retrieve the artefact just updated
		jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
		
		
		
		urlToCall = sharePointSiteUri + SharePointUrlMaker.getListByTitleItemById(trackerName, 4);
		log.info("retrieve the fourth artefact to update");
		// retrieve the artefact just updated
		jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
		etag = jsonObjectResult.getJSONObject("d").getJSONObject("__metadata").getString("etag");

		log.info("update the actual and remaining efforts");
		// update the assignation
		jsonObject = new JSONObject("{'__metadata':{'type':'SP.Data."+trackerName+"ListItem', 'etag':'" + etag + "'},'assignedToUsernameId':'163'}");
		jsonObjectResult = SharePointClientUtil.patchRequest(urlToCall, restTemplate, token, formDigest, jsonObject, etag);
		
		log.info("retrieve the fourth artefact just updated");
		// retrieve the artefact just updated
		jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
		
		
		
		log.info("retrieve the artefact just updated");
		urlToCall = sharePointSiteUri + SharePointUrlMaker.getListByTitleItemById(trackerName, newStickyNoteIndex);
		// retrieve the artefact just updated
		jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
		etag = jsonObjectResult.getJSONObject("d").getJSONObject("__metadata").getString("etag");

		log.info("delete the previously created StickyNote");
		// delete the previously created StickyNote
		jsonObjectResult = SharePointClientUtil.deleteRequest(urlToCall, restTemplate, token, formDigest, etag);
	}
	
	private String generateStickyNote(String trackerName, String title, String description, String estimatedEffort, String actualEffort,
			String remainingEffort, String category, String priority, String assignedToUsernameId) {
		String result = "";
		result += "{";
		result += "		'__metadata':{'type':'SP.Data." + trackerName + "ListItem', 'etag':'1'},";
		result += "		'Title':'" + title + "',";
		result += "		'description':'" + description + "',";
		result += "		'estimatedEffort':'" + estimatedEffort + "',";
		result += "		'actualEffort':'" + actualEffort + "',";
		result += "		'remainingEffort':'" + remainingEffort + "',";
		result += "		'category':'" + category + "',";
		result += "		'priority':'" + priority + "',";
		result += "		'assignedToUsernameId':'" + assignedToUsernameId + "'";
		result += "}";
		return result;
	}
}
