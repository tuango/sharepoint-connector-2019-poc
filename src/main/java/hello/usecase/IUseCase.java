package hello.usecase;

import org.json.JSONException;

import com.capgemini.connector.sharepoint.exception.SharePointClientException;

public interface IUseCase {
	public void run() throws JSONException, SharePointClientException;
}
