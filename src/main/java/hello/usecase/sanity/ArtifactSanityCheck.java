package hello.usecase.sanity;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.client.RestTemplate;

import com.capgemini.connector.sharepoint.exception.SharePointClientException;
import com.capgemini.connector.sharepoint.token.JsonWebTokenGenerator;
import com.capgemini.connector.sharepoint.util.SharePointClientUtil;
import com.capgemini.connector.sharepoint.util.SharePointODataOperator;
import com.capgemini.connector.sharepoint.util.SharePointUrlMaker;

import hello.usecase.IUseCase;
import lombok.Data;

public class ArtifactSanityCheck implements IUseCase {

	private static final Logger log = Logger.getLogger(ArtifactSanityCheck.class);

	private RestTemplate restTemplate;
	private URI sharePointSiteUri;
	private String trackerId;
	private JsonWebTokenGenerator tokenGenerator;
	private String nameId;
	
	@Data
	private static class Artifact {
		private Long artifactTechnicalId;
		private String title;
		private String artifactId;
		
		public Artifact(Long artifactTechnicalId, String title, String artifactId) {
			super();
			this.artifactTechnicalId = artifactTechnicalId;
			this.title = title;
			this.artifactId = artifactId;
		}
		
		@Override
		public String toString() {
			return "[" + this.getArtifactTechnicalId() +", " + this.getTitle() + ", " + this.getArtifactId() + "]";
		}
	}


	public ArtifactSanityCheck(RestTemplate restTemplate, URI sharePointSiteUri,
			JsonWebTokenGenerator tokenGenerator, String nameId, String trackerId) {
		super();
		this.restTemplate = restTemplate;
		this.sharePointSiteUri = sharePointSiteUri;
		this.trackerId = trackerId;
		this.tokenGenerator = tokenGenerator;
		this.nameId = nameId;
	}

	@Override
	public void run() throws JSONException, SharePointClientException {
		String urlToCall;
		String token = tokenGenerator.getToken(nameId);
		
		JSONObject jsonObjectResult;

		List<Artifact> allItems = getAllItems(restTemplate, token, nameId);
		
		log.info("JWT:" + token);
		
		log.info("All items:" + allItems);
		
		
	}
	
	
	
	private List<Artifact> getAllItems(RestTemplate restTemplate, String token, String nameId) throws JSONException, SharePointClientException {
		String urlToCall = sharePointSiteUri + SharePointUrlMaker.getListByTitleItems(this.trackerId);
		JSONObject jsonObject = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
		log.info("All items of specific tracker : " + this.trackerId);
		JSONArray jsonResults = jsonObject.getJSONArray("value");
		
		List<Artifact> items = new ArrayList<>();
		
		for (int i = 0; i < jsonResults.length(); i++ ) {
			JSONObject aJsonObject = jsonResults.getJSONObject(i);
			if (aJsonObject.has("Id")) {
				Long artifactTechnicalId = aJsonObject.getLong("Id");
				String title = aJsonObject.getString("Title");				
				items.add(new Artifact(artifactTechnicalId, title, null));				
			}
		}
		log.info("items.size : " + items.size());
		return items;
	}
}
