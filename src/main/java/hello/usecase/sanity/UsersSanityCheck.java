package hello.usecase.sanity;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.client.RestTemplate;

import com.capgemini.connector.sharepoint.exception.SharePointClientException;
import com.capgemini.connector.sharepoint.token.JsonWebTokenGenerator;
import com.capgemini.connector.sharepoint.util.SharePointClientUtil;
import com.capgemini.connector.sharepoint.util.SharePointUrlMaker;

import hello.usecase.IUseCase;

public class UsersSanityCheck implements IUseCase {

	private static final Logger log = Logger.getLogger(UsersSanityCheck.class);

	private RestTemplate restTemplate;
	private URI sharePointSiteUri;
	private String trackerId;
	private JsonWebTokenGenerator tokenGenerator;
	private String nameId;

	private static class User {
		private String title;
		private String nameId;
		
		public User(String title, String nameId) {
			super();
			this.title = title;
			this.nameId = nameId;
		}
		
		public String getTitle() {
			return title;
		}
		public String getNameId() {
			return nameId;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((nameId == null) ? 0 : nameId.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			User other = (User) obj;
			if (nameId == null) {
				if (other.nameId != null) {
					return false;
				}
			} else if (!nameId.equals(other.nameId)) {
				return false;
			}
			return true;
		}
		
		@Override
		public String toString() {
			return "[" + this.getNameId() +", " + this.getTitle() + "]";
		}
	}

	public UsersSanityCheck(RestTemplate restTemplate, URI sharePointSiteUri,
			JsonWebTokenGenerator tokenGenerator, String nameId, String trackerId) {
		super();
		this.restTemplate = restTemplate;
		this.sharePointSiteUri = sharePointSiteUri;
		this.tokenGenerator = tokenGenerator;
		this.nameId = nameId;
		this.trackerId = trackerId;
	}

	@Override
	public void run() throws JSONException, SharePointClientException {
		String urlToCall;
		String token = tokenGenerator.getToken(nameId);
		JSONObject jsonObjectResult;

		List<User> allUsers = getAllUsers(restTemplate, token, nameId);
		List<User> passedUsers = new ArrayList<User>();
		List<User> failedUsers = new ArrayList<User>();
		
		log.info(allUsers);
	}

	private List<User> getAllUsers(RestTemplate restTemplate, String token, String nameId) throws JSONException, SharePointClientException {
		List<User> result = new ArrayList<User>();
		String urlToCall = sharePointSiteUri + SharePointUrlMaker.getSiteUsers();
		JSONObject jsonObject = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
		log.info("AllUsers : " + jsonObject);
		JSONArray jsonResults = jsonObject.getJSONArray("value");
		for (int i = 0; i < jsonResults.length(); i++ ) {
			JSONObject aJsonObject = jsonResults.getJSONObject(i);
			if (aJsonObject.has("UserId") && !aJsonObject.isNull("UserId")) {
				String theNameId = aJsonObject.getJSONObject("UserId").getString("NameId");
				String theTitle = aJsonObject.getString("Title");
				User user = new User(theTitle, theNameId.toUpperCase());
				if (!result.contains(user)) {
					result.add(user);
				} else {
					log.info("duplicated user : " + theNameId.toUpperCase());
				}
			}
		}
		log.info("AllUsers.size : " + result.size());
		return result;
	}
}
