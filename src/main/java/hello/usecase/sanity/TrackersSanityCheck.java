package hello.usecase.sanity;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.client.RestTemplate;

import com.capgemini.connector.sharepoint.exception.SharePointClientException;
import com.capgemini.connector.sharepoint.token.JsonWebTokenGenerator;
import com.capgemini.connector.sharepoint.util.SharePointClientUtil;
import com.capgemini.connector.sharepoint.util.SharePointODataOperator;
import com.capgemini.connector.sharepoint.util.SharePointUrlMaker;

import hello.usecase.IUseCase;

public class TrackersSanityCheck implements IUseCase {

	private static final Logger log = Logger.getLogger(TrackersSanityCheck.class);

	private RestTemplate restTemplate;
	private URI sharePointSiteUri;
	private String trackerId;
	private JsonWebTokenGenerator tokenGenerator;
	private String nameId;

	private static class Tracker {
		private String trackerName;
		private String trackerId;
		
		public Tracker(String trackerName, String trackerId) {
			super();
			this.trackerName = trackerName;
			this.trackerId = trackerId;
		}
		
		public String getTrackerName() {
			return trackerName;
		}

		public String getTrackerId() {
			return trackerId;
		}

		@Override
		public String toString() {
			return "[" + this.getTrackerId() +", " + this.getTrackerName() + "]";
		}
	}

	public TrackersSanityCheck(RestTemplate restTemplate, URI sharePointSiteUri,
			JsonWebTokenGenerator tokenGenerator, String nameId, String trackerId) {
		super();
		this.restTemplate = restTemplate;
		this.sharePointSiteUri = sharePointSiteUri;
		this.trackerId = trackerId;
		this.tokenGenerator = tokenGenerator;
		this.nameId = nameId;
	}

	@Override
	public void run() throws JSONException, SharePointClientException {
		String urlToCall;
		String token = tokenGenerator.getToken(nameId);
		JSONObject jsonObjectResult;

		List<Tracker> allTrackers = getAllTrackers(restTemplate, token, nameId);
		List<Tracker> passedTrackers = new ArrayList<Tracker>();
		List<Tracker> failedTrackers = new ArrayList<Tracker>();
		for (Tracker aTracker : allTrackers) {
			SharePointODataOperator oDataOperator = new SharePointODataOperator("Member/Title,Member/UserId,Member/Email,Member/Users/Title,Member/Users/UserId,Member/Users/Email", null, "member/users", null);
			urlToCall = sharePointSiteUri + SharePointUrlMaker.getListByTitleRoleAssignments(aTracker.getTrackerId(), oDataOperator);
			jsonObjectResult = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
			if (jsonObjectResult != null) {
				passedTrackers.add(aTracker);
				log.info("Users can access to tracker [" + aTracker.getTrackerId() + "] are : ");
				for (String user : toUserList(jsonObjectResult)) {
					log.info(user);
				}
			} else {
				failedTrackers.add(aTracker);
			}
		}

		log.info("KO for Trackers : ");
		for (Tracker aTracker : failedTrackers) {
			log.info(aTracker);
		}
		
		log.info("OK for Trackers : ");
		for (Tracker aTracker : passedTrackers) {
			log.info(aTracker);
		}
	}
	
	private Set<String> toUserList(JSONObject jsonObject) {
		Set<String> users = new HashSet<String>();
		
		JSONArray jsonResults = jsonObject.getJSONObject("d").getJSONArray("results");
		// loop over SP.roleAssignments
		for (int i = 0; i < jsonResults.length(); i++ ) {
			users.addAll(roleAssignmentToUserList(jsonResults.getJSONObject(i)));
		}
		return users;
	}

	private Set<String> roleAssignmentToUserList(JSONObject jsonRoleAssignment) {
		Set<String> users = new HashSet<String>();
		
		JSONObject member = jsonRoleAssignment.getJSONObject("Member");
		String roleAssignmentMemberType = member.getJSONObject("__metadata").getString("type");
		if ("SP.Group".equals(roleAssignmentMemberType)) {
			users.addAll(groupToUserList(member));
		}
		if ("SP.User".equals(roleAssignmentMemberType)) {
			users.add(userToUser(member));
		}
		
		return users;
	}
	
	private List<String> groupToUserList(JSONObject jsonGroup) {
		List<String> users = new ArrayList<String>();
		
		JSONArray jsonUsersArray = jsonGroup.getJSONObject("Users").getJSONArray("results");
		// loop over SP.roleAssignments
		for (int i = 0; i < jsonUsersArray.length(); i++ ) {
			JSONObject jsonUser = jsonUsersArray.getJSONObject(i);
			users.add(userToUser(jsonUser));
		}
		return users;
	}
	
	private String userToUser(JSONObject jsonUser) {
		return jsonUser.getString("Title");
	}
	
	private List<Tracker> getAllTrackers(RestTemplate restTemplate, String token, String nameId) throws JSONException, SharePointClientException {
		List<Tracker> result = new ArrayList<Tracker>();
		String urlToCall = sharePointSiteUri + SharePointUrlMaker.getListByTitleItems(this.trackerId);
		JSONObject jsonObject = SharePointClientUtil.getRequest(urlToCall, restTemplate, token);
		log.info("AllTrackers : " + jsonObject);
		JSONArray jsonResults = jsonObject.getJSONArray("value");
		for (int i = 0; i < jsonResults.length(); i++ ) {
			JSONObject aJsonObject = jsonResults.getJSONObject(i);
			if (aJsonObject.has("TrackerId")) {
				String theTrackerId = aJsonObject.getString("TrackerId");
				String theTrackerName = aJsonObject.getString("TrackerName");
				Tracker tracker = new Tracker(theTrackerName, theTrackerId);
				if (!result.contains(tracker)) {
					result.add(tracker);
				} else {
					log.info("duplicated tracker : " + theTrackerId);
				}
			}
		}
		log.info("AllTrackers.size : " + result.size());
		return result;
	}
}
